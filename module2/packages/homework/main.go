package main

import (
	"fmt"

	"gitlab.com/Krokin/greet/greet"
)

func main() {
	greet.Hello()
	oct := greet.Octopus{
		Name:  "Grom",
		Color: "Green",
	}
	fmt.Println(oct.String())
	oct.Reset()
	fmt.Println(oct.String())
}
