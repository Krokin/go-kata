package main

import (
	"reflect"
	"testing"
)

func Test_calc_Do_Sum(t *testing.T) {
	type fields struct {
		a float64
		b float64
	}
	type args struct {
		operation func(a, b float64) float64
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *calc
	}{
		{
			name:   "TEST SUM CASE 1",
			fields: fields{10, 34},
			args:   args{sum},
			want:   &calc{10, 34, 44},
		},
		{
			name:   "TEST SUM CASE 2",
			fields: fields{0, 10},
			args:   args{sum},
			want:   &calc{0, 10, 10},
		},
		{
			name:   "TEST SUM CASE 3",
			fields: fields{1002, -302},
			args:   args{sum},
			want:   &calc{1002, -302, 700},
		},
		{
			name:   "TEST SUM CASE 4",
			fields: fields{25, 0.5},
			args:   args{sum},
			want:   &calc{25, 0.5, 25.5},
		},
		{
			name:   "TEST SUM CASE 5",
			fields: fields{1, -2001},
			args:   args{sum},
			want:   &calc{1, -2001, -2000},
		},
		{
			name:   "TEST SUM CASE 6",
			fields: fields{-5, -5},
			args:   args{sum},
			want:   &calc{-5, -5, -10},
		},
		{
			name:   "TEST SUM CASE 7",
			fields: fields{10, 34},
			args:   args{sum},
			want:   &calc{10, 34, 44},
		},
		{
			name:   "TEST SUM CASE 8",
			fields: fields{-12.5, 2.5},
			args:   args{sum},
			want:   &calc{-12.5, 2.5, -10},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &calc{}
			c.SetA(tt.fields.a).SetB(tt.fields.b).Do(tt.args.operation)
			if !reflect.DeepEqual(c, tt.want) {
				t.Errorf("Do() = %v, want %v", c, tt.want)
			}
		})
	}
}

func Test_calc_Do_Divide(t *testing.T) {
	type fields struct {
		a float64
		b float64
	}
	type args struct {
		operation func(a, b float64) float64
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *calc
	}{
		{
			name:   "TEST DIVIDE CASE 1",
			fields: fields{10, 2},
			args:   args{divide},
			want:   &calc{10, 2, 5},
		},
		{
			name:   "TEST DIVIDE CASE 2",
			fields: fields{0, 10},
			args:   args{divide},
			want:   &calc{0, 10, 0},
		},
		{
			name:   "TEST DIVIDE CASE 3",
			fields: fields{-100, 5},
			args:   args{divide},
			want:   &calc{-100, 5, -20},
		},
		{
			name:   "TEST DIVIDE CASE 4",
			fields: fields{5, -100},
			args:   args{divide},
			want:   &calc{5, -100, -0.05},
		},
		{
			name:   "TEST DIVIDE CASE 5",
			fields: fields{-5, -100},
			args:   args{divide},
			want:   &calc{-5, -100, 0.05},
		},
		{
			name:   "TEST DIVIDE CASE 6",
			fields: fields{9.3, 3},
			args:   args{divide},
			want:   &calc{9.3, 3, 3.1},
		},
		{
			name:   "TEST DIVIDE CASE 7",
			fields: fields{100000, 100000},
			args:   args{divide},
			want:   &calc{100000, 100000, 1},
		},
		{
			name:   "TEST DIVIDE CASE 8",
			fields: fields{5, 10},
			args:   args{divide},
			want:   &calc{5, 10, 0.5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &calc{}
			c.SetA(tt.fields.a).SetB(tt.fields.b).Do(tt.args.operation)
			if !reflect.DeepEqual(c, tt.want) {
				t.Errorf("Do() = %v, want %v", c, tt.want)
			}
		})
	}
}

func Test_calc_Do_Multiply(t *testing.T) {
	type fields struct {
		a float64
		b float64
	}
	type args struct {
		operation func(a, b float64) float64
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *calc
	}{
		{
			name:   "TEST MULTIPLY CASE 1",
			fields: fields{10, 2},
			args:   args{multiply},
			want:   &calc{10, 2, 20},
		},
		{
			name:   "TEST MULTIPLY CASE 2",
			fields: fields{0, 10},
			args:   args{multiply},
			want:   &calc{0, 10, 0},
		},
		{
			name:   "TEST MULTIPLY CASE 3",
			fields: fields{-100, 5},
			args:   args{multiply},
			want:   &calc{-100, 5, -500},
		},
		{
			name:   "TEST MULTIPLY CASE 4",
			fields: fields{5, -100},
			args:   args{multiply},
			want:   &calc{5, -100, -500},
		},
		{
			name:   "TEST MULTIPLY CASE 5",
			fields: fields{-5, -100},
			args:   args{multiply},
			want:   &calc{-5, -100, 500},
		},
		{
			name:   "TEST MULTIPLY CASE 6",
			fields: fields{9.5, 4},
			args:   args{multiply},
			want:   &calc{9.5, 4, 38},
		},
		{
			name:   "TEST MULTIPLY CASE 7",
			fields: fields{100000, 100000},
			args:   args{multiply},
			want:   &calc{100000, 100000, 1e+10},
		},
		{
			name:   "TEST MULTIPLY CASE 8",
			fields: fields{5, 10},
			args:   args{multiply},
			want:   &calc{5, 10, 50},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &calc{}
			c.SetA(tt.fields.a).SetB(tt.fields.b).Do(tt.args.operation)
			if !reflect.DeepEqual(c, tt.want) {
				t.Errorf("Do() = %v, want %v", c, tt.want)
			}
		})
	}
}

func Test_calc_Do_Average(t *testing.T) {
	type fields struct {
		a float64
		b float64
	}
	type args struct {
		operation func(a, b float64) float64
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *calc
	}{
		{
			name:   "TEST AVERAGE CASE 1",
			fields: fields{10, 2},
			args:   args{average},
			want:   &calc{10, 2, 6},
		},
		{
			name:   "TEST AVERAGE CASE 2",
			fields: fields{0, 10},
			args:   args{average},
			want:   &calc{0, 10, 5},
		},
		{
			name:   "TEST AVERAGE CASE 3",
			fields: fields{-100, 6},
			args:   args{average},
			want:   &calc{-100, 6, -47},
		},
		{
			name:   "TEST AVERAGE CASE 4",
			fields: fields{6, -100},
			args:   args{average},
			want:   &calc{6, -100, -47},
		},
		{
			name:   "TEST AVERAGE CASE 5",
			fields: fields{-6, -100},
			args:   args{average},
			want:   &calc{-6, -100, -53},
		},
		{
			name:   "TEST AVERAGE CASE 6",
			fields: fields{9.5, 0.5},
			args:   args{average},
			want:   &calc{9.5, 0.5, 5},
		},
		{
			name:   "TEST AVERAGE CASE 7",
			fields: fields{100000, 100000},
			args:   args{average},
			want:   &calc{100000, 100000, 100000},
		},
		{
			name:   "TEST AVERAGE CASE 8",
			fields: fields{5, 10},
			args:   args{average},
			want:   &calc{5, 10, 7.5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &calc{}
			c.SetA(tt.fields.a).SetB(tt.fields.b).Do(tt.args.operation)
			if !reflect.DeepEqual(c, tt.want) {
				t.Errorf("Do() = %v, want %v", c, tt.want)
			}
		})
	}
}
