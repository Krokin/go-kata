package main

import (
	"testing"
)

func BenchmarkMarshalJson(b *testing.B) {
	c := Cats{
		ID:        1,
		Category:  Category{ID: 25, Name: "oks"},
		Name:      "Dogis",
		PhotoUrls: []string{"google.com", "youtube.com"},
		Tags: []Category{{
			ID:   1,
			Name: "big",
		}, {
			ID:   2,
			Name: "small",
		}},
		Status: "200",
	}
	var (
		data []byte
		err  error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = c.MarshalJson()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkMarshalJsoniter(b *testing.B) {
	ca := Cats{
		ID:        1,
		Category:  Category{ID: 25, Name: "oks"},
		Name:      "Dogis",
		PhotoUrls: []string{"google.com", "youtube.com"},
		Tags: []Category{{
			ID:   1,
			Name: "big",
		}, {
			ID:   2,
			Name: "small",
		}},
		Status: "200",
	}
	var (
		data []byte
		err  error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = ca.MarshalJsoniter()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}
