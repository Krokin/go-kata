package main

import (
	"encoding/json"
	"testing"
)

var cats = Cats{
	ID:        1,
	Category:  Category{ID: 25, Name: "oks"},
	Name:      "Dogis",
	PhotoUrls: []string{"google.com", "youtube.com"},
	Tags: []Category{{
		ID:   1,
		Name: "big",
	}, {
		ID:   2,
		Name: "small",
	}},
	Status: "200",
}
var jsonData, _ = json.Marshal(cats)

func BenchmarkUnmarshalJson(b *testing.B) {
	var (
		c   Cats
		err error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		c, err = UnmarshalCatsJson(jsonData)
		if err != nil {
			panic(err)
		}
		_ = c
	}
}

func BenchmarkUnmarshalJsoniter(b *testing.B) {
	var (
		c   Cats
		err error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		c, err = UnmarshalCatsJsoniter(jsonData)
		if err != nil {
			panic(err)
		}
		_ = c
	}
}
