package main

import "testing"

func BenchmarkSanitizeText(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			SanitizeText(data[i])
		}
	}
}

func BenchmarkSanitizeText1(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeTextOpt1(data[i])
		}
	}
}

func BenchmarkSanitizeText2(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeTextOpt2(data[i])
		}
	}
}
