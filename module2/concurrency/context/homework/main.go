package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) <-chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func main() {

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)

	go func() {
		for _, num := range []int{1, 2, 3} {
			select {
			case <-ctx.Done():
				close(a)
				cancel()
				return
			case a <- num:
			}
		}
		<-ctx.Done()
		close(a)
	}()

	go func() {
		for _, num := range []int{20, 10, 30} {
			select {
			case <-ctx.Done():
				close(b)
				cancel()
				return
			case b <- num:
			}
		}
		<-ctx.Done()
		close(b)
	}()

	go func() {
		for _, num := range []int{300, 200, 100} {
			select {
			case <-ctx.Done():
				close(c)
				cancel()
				return
			case c <- num:
			}
		}
		<-ctx.Done()
		close(c)
	}()

	for num := range joinChannels(a, b, c) {
		fmt.Println(num)
	}

}
