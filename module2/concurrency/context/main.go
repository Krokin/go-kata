package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

type key int

const (
	userIDctx key = 0
)

func main() {
	http.HandleFunc("/", handler)
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	id := r.Header.Get("User-ID")

	ctx := context.WithValue(r.Context(), userIDctx, id)

	str := processLongTask(ctx)

	_, err := w.Write([]byte(str))
	if err != nil {
		log.Fatal(err)
	}
}

func processLongTask(ctx context.Context) string {
	id := ctx.Value(userIDctx)
	select {
	case <-time.After(2 * time.Second):
		return fmt.Sprintf("Ok, %s\n", id)
	case <-ctx.Done():
		log.Println("reject")
		return ""
	}
}
