package main

import (
	"fmt"
	"time"
)

func main() {
	msg1 := make(chan string)
	msg2 := make(chan string)

	go func() {
		for {
			time.Sleep(500 * time.Millisecond)
			msg1 <- "Полсекунды прошло"
		}
	}()

	go func() {
		for {
			time.Sleep(2 * time.Second)
			msg2 <- "2 Cекунды прошло"
		}
	}()

	for {
		select {
		case msg := <-msg1:
			fmt.Println(msg)
		case msg := <-msg2:
			fmt.Println(msg)
		}
	}
}
