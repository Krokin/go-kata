package main

import (
	"fmt"
	"sync"
	"time"
)

const timer = 30 * time.Second

func joinChannels(chs ...<-chan int) <-chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func multiTiker(t time.Duration) (<-chan struct{}, <-chan struct{}, <-chan struct{}) {
	tR := time.NewTicker(t)
	t1, t2, t3 := make(chan struct{}), make(chan struct{}), make(chan struct{})
	go func() {
		defer func() {
			close(t1)
			close(t2)
			close(t3)
		}()

		for range tR.C {
			t1 <- struct{}{}
			t2 <- struct{}{}
			t3 <- struct{}{}
			return
		}
	}()
	return t1, t2, t3
}

func main() {

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)
	t1, t2, t3 := multiTiker(timer)
	go func() {
		for _, num := range []int{1, 2, 3} {
			select {
			case <-t1:
				close(a)
				return
			case a <- num:
			}
		}
		<-t1
		close(a)
	}()

	go func() {
		for _, num := range []int{20, 10, 30} {
			select {
			case <-t2:
				close(b)
				return
			case b <- num:
			}
		}
		<-t2
		close(b)
	}()

	go func() {
		for _, num := range []int{300, 200, 100} {
			select {
			case <-t3:
				close(c)
				return
			case c <- num:
			}
		}
		<-t3
		close(c)
	}()

	for num := range joinChannels(a, b, c) {
		fmt.Println(num)
	}
}
