package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	buffer := new(bytes.Buffer)
	buffer.WriteString(strings.Join(data, "\n"))

	f, err := os.Create("module2/stl/io/homework/example.txt")
	check(err)
	defer f.Close()

	_, err = io.Copy(f, buffer)
	check(err)

	err = f.Sync()
	check(err)

	_, err = f.Seek(0, 0)
	check(err)

	resBuffer := new(bytes.Buffer)
	_, err = resBuffer.ReadFrom(f)
	check(err)

	fmt.Println(resBuffer.String())
}
