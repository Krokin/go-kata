package main

import "fmt"

func main() {
	fmt.Println(generateSelfStory("Vlad", 25, 10.00000025))
}

func generateSelfStory(name string, age int, money float64) string {
	return fmt.Sprintf("My name is %s. I'm %d y.o. And I also have $%.2f in my wallet right now.", name, age, money)
}
