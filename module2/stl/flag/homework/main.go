package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app"`
	Production bool
}

func main() {
	var path string
	flag.StringVar(&path, "conf", "", "path JSON")
	flag.Parse()

	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}

	conf := Config{}
	if err := json.NewDecoder(f).Decode(&conf); err != nil {
		panic(err)
	}

	fmt.Printf("Production: %t\nAppName: %s\n", conf.Production, conf.AppName)
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
