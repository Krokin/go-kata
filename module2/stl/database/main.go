package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "./module2/stl/database/gopher.db")

	statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	if err != nil {
		panic(err)
	}
	result, _ := statement.Exec()
	_ = result

	statement, err = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
	if err != nil {
		panic(err)
	}
	result, _ = statement.Exec("Mojenk", "Bob")
	_ = result

	rows, err := database.Query("SELECT * FROM people")
	if err != nil {
		panic(err)
	}

	var (
		id                  int
		firstname, lastname string
	)

	for rows.Next() {
		err := rows.Scan(&id, &firstname, &lastname)
		if err != nil {
			panic(err)
		}
		fmt.Println(id, lastname, firstname)
	}
}
