package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// В задании непонятно в какой файл писать, импровизация)
func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your data: ")

	name, err := reader.ReadString('\n')
	check(err)

	f, err := os.Create("test.txt")
	check(err)
	defer f.Close()

	_, err = f.WriteString(name)
	check(err)
}
