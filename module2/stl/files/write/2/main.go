package main

import (
	"io"
	"os"
	"unicode/utf8"
)

var baseRuEn = map[rune]string{
	'а': "a", 'А': "A", 'Б': "B", 'б': "b", 'В': "V", 'в': "v", 'Г': "G", 'г': "g",
	'Д': "D", 'д': "d", 'Е': "E", 'е': "e", 'Ё': "yO", 'ё': "yo", 'Ж': "J", 'ж': "j",
	'З': "Z", 'з': "z", 'И': "I", 'и': "i", 'Й': "Y'O", 'й': "y'o", 'К': "K", 'к': "k",
	'Л': "L", 'л': "l", 'М': "M", 'м': "m", 'Н': "N", 'н': "n", 'О': "O", 'о': "o",
	'П': "P", 'п': "p", 'Р': "R", 'р': "r", 'С': "S", 'с': "s", 'Т': "T", 'т': "t",
	'У': "U", 'у': "u", 'Ф': "F", 'ф': "f", 'Х': "H", 'х': "h", 'Ц': "CZ", 'ц': "cz",
	'Ч': "CH", 'ч': "ch", 'Ш': "SH", 'ш': "sh", 'Щ': "SH'", 'щ': "sh'", 'Ъ': "'", 'ъ': "'",
	'Ы': "I", 'ы': "i", 'Ь': "'", 'ь': "'", 'Э': "YE", 'э': "ye", 'Ю': "YU", 'ю': "yu", 'Я': "YA", 'я': "ya",
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	f, err := os.Open("module2/stl/files/write/2/example.txt")
	check(err)
	defer f.Close()

	data, err := io.ReadAll(f)
	check(err)

	str := translit(data)
	f2, err := os.Create("module2/stl/files/write/2/example.processed.txt")
	check(err)
	defer f2.Close()

	_, err = f2.WriteString(str)
	check(err)
}

func translit(data []byte) string {
	var res string
	for len(data) > 0 {
		r, size := utf8.DecodeRune(data)
		if symbol, ok := baseRuEn[r]; ok {
			res += symbol
		} else {
			res += string(r)
		}
		data = data[size:]
	}
	return res
}
