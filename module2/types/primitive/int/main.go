package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeInt()
}

func typeInt() {
	fmt.Println("== Start ==")
	{
		var uintNumber uint8 = 1 << 7
		var min = int8(uintNumber)
		uintNumber--
		var max = int8(uintNumber)
		fmt.Println("int 8 min:", min, "max:", max, "size:", unsafe.Sizeof(min), "bytes")
	}
	{
		var uintNumber uint16 = 1 << 15
		var min = int16(uintNumber)
		uintNumber--
		var max = int16(uintNumber)
		fmt.Println("int 16 min:", min, "max:", max, "size:", unsafe.Sizeof(min), "bytes")
	}
	{
		var uintNumber uint32 = 1 << 31
		var min = int32(uintNumber)
		uintNumber--
		var max = int32(uintNumber)
		fmt.Println("int 32 min:", min, "max:", max, "size:", unsafe.Sizeof(min), "bytes")
	}
	{
		var uintNumber uint64 = 1 << 63
		var min = int64(uintNumber)
		uintNumber--
		var max = int64(uintNumber)
		fmt.Println("int 64 min:", min, "max:", max, "size:", unsafe.Sizeof(min), "bytes")
	}
	fmt.Println("== END ==")
}
