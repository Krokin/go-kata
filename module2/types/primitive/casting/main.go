package main

import "fmt"

func main() {
	var b float32 = 55.5
	var res = int(b)
	fmt.Printf("type: %T values: %v", res, res)
}
