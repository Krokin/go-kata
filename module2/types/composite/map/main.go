package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/moovweb/gvm",
			Stars: 8100,
		},
		{
			Name:  "https://github.com/microsoft/WSL",
			Stars: 14400,
		},
		{
			Name:  "https://github.com/Miffyli/gan-aimbots",
			Stars: 16,
		},
		{
			Name:  "https://github.com/mochi-co/mqtt",
			Stars: 372,
		},
		{
			Name:  "https://github.com/Krokin/Kata_Golang_Calc",
			Stars: 0,
		},
		{
			Name:  "https://github.com/microsoft/vscode-go",
			Stars: 6000,
		},
		{
			Name:  "https://github.com/golangci/golangci-lint",
			Stars: 11700,
		},
		{
			Name:  "https://github.com/Priler/aimlabbot",
			Stars: 29,
		},
		{
			Name:  "https://github.com/AltimorTASDK/SFVNetcodeFix",
			Stars: 150,
		},
		{
			Name:  "https://github.com/asdf-vm/asdf",
			Stars: 16400,
		},
		{
			Name:  "https://github.com/Lidemy/mentor-program-aszx87410",
			Stars: 3,
		},
		{
			Name:  "https://github.com/xuxueli/xxl-job",
			Stars: 23300,
		},
	}

	// в цикле запишите в map
	var gitProjects = make(map[string]Project)
	for _, val := range projects {
		gitProjects[val.Name] = val
	}

	// в цикле пройдитесь по мапе и выведите значения в консоль
	for _, val := range gitProjects {
		fmt.Println(val)
	}
}
