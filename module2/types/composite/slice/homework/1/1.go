package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s = Append(s)
	fmt.Println(s)
	s2 := []int{1, 2, 3}
	Append2(&s2)
	fmt.Println(s2)
}

func Append(s []int) []int {
	s = append(s, 4)
	return s
}

func Append2(s2 *[]int) {
	*s2 = append(*s2, 4)
}
