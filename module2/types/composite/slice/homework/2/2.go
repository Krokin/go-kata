package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	users = remove(users)
	fmt.Println(users)
}

func remove(users []User) []User {
	var newUsers = make([]User, 0, len(users))
	for _, user := range users {
		if user.Age <= 40 {
			newUsers = append(newUsers, user)
		}
	}
	return newUsers
}
