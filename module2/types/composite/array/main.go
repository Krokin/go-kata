package main

import (
	"fmt"
)

type User struct {
	Age    int
	Name   string
	Wallet Wallet
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	typeArray()
	rangeArray()
}

func typeArray() {
	a := [...]int{34, 55, 89, 144}
	fmt.Println("original array: ", a)
	a[0] = 21
	fmt.Println("change first elem array: ", a)
	b := a
	a[0] = 233
	fmt.Println("original array: ", a)
	fmt.Println("modified array: ", b)
}

func rangeArray() {
	users := [4]User{
		{
			Age:  13,
			Name: "John",
			Wallet: Wallet{
				RUB: 333321,
				USD: 2333,
				BTC: 7,
				ETH: 15,
			},
		},
		{
			Age:  17,
			Name: "Anna",
			Wallet: Wallet{
				RUB: 11233,
				USD: 324,
				BTC: 2,
				ETH: 5,
			},
		},
		{
			Age:  35,
			Name: "Mishanya",
			Wallet: Wallet{
				RUB: 777891,
				USD: 5523,
				BTC: 124,
				ETH: 2,
			},
		},
		{
			Age:  44,
			Name: "Svetlana",
			Wallet: Wallet{
				RUB: 111200,
				USD: 120,
				BTC: 555,
				ETH: 2,
			},
		},
	}
	fmt.Println("Пользователи старше 18 лет: ")
	for i := range users {
		if users[i].Age > 18 {
			fmt.Println(users[i])
		}
	}
	fmt.Println("Крипто киты: ")
	for i, user := range users {
		if user.Wallet.ETH > 0 || user.Wallet.BTC > 0 {
			fmt.Println("user: ", user, "index: ", i)
		}
	}
	fmt.Println("====== ENDS USERS NON ZERO CRYPTO ======")
}
