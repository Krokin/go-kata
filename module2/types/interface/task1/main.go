package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n interface{}
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch x := r.(type) {
	case nil:
		fmt.Println("Success!")
	default:
		fmt.Printf("type: %T", x)
	}
}
