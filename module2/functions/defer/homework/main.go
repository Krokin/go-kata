package main

import (
	"errors"
	"fmt"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func main() {
	jobs := MergeDictsJob{
		Dicts:      []map[string]string{{"a": "lol", "b": "ss", "c": "kiss"}, {"d": "memo", "e": "ehoho", "f": "finger"}},
		Merged:     map[string]string{},
		IsFinished: false,
	}

	res, err := ExecuteMergeDictsJob(&jobs)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(res)
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	defer func() {
		job.IsFinished = true
	}()

	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}

	for _, dict := range job.Dicts {
		if dict == nil {
			return job, errNilDict
		}

		for key, val := range dict {
			job.Merged[key] = val
		}
	}

	return job, nil
}

// END

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{}) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
