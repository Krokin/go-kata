package main

import (
	"fmt"
	"testing"
)

func TestGreet(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "english name",
			args: args{name: "John"},
			want: "Hello John, you welcome!",
		},
		{
			name: "english name",
			args: args{name: "Antonio"},
			want: "Hello Antonio, you welcome!",
		},
		{
			name: "english name",
			args: args{name: "Greg"},
			want: "Hello Greg, you welcome!",
		},
		{
			name: "english name",
			args: args{name: "Kate"},
			want: "Hello Kate, you welcome!",
		},
		{
			name: "russian name",
			args: args{name: "Боря"},
			want: "Привет Боря, добро пожаловать!",
		},
		{
			name: "russian name",
			args: args{name: "Маша"},
			want: "Привет Маша, добро пожаловать!",
		},
		{
			name: "russian name",
			args: args{name: "Ян"},
			want: "Привет Ян, добро пожаловать!",
		},
		{
			name: "russian name",
			args: args{name: "Антон"},
			want: "Привет Антон, добро пожаловать!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Greet(tt.args.name); got != tt.want {
				t.Errorf("Greet() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Greet(name string) string {
	i := 0
	for range name {
		i++
	}
	if len(name) != i {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	}
	return fmt.Sprintf("Hello %s, you welcome!", name)
}
