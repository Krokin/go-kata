package main

import (
	"testing"
)

var users []User = genUsers()
var products []Product = genProducts()

func BenchmarkMapStandart(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkMapOptimize(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}

func BenchmarkMapSimple(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts3(users, products)
	}
}
