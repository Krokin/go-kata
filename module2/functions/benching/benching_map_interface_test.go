package benching

import (
	"testing"
)

func BenchmarkInsertInterfaceMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXInterfaceMap(100000, b)
	}
}

// BenchmarkInsertIntMap10000 тестирует скорость вставки 10000 целых чисел в карту.
func BenchmarkInsertInterfaceMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXInterfaceMap(10000, b)
	}
}

// BenchmarkInsertInterfaceMap1000 тестирует скорость вставки 1000 целых чисел в карту.
func BenchmarkInsertInterfaceMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXInterfaceMap(1000, b)
	}
}

// BenchmarkInsertIntMap100 тестирует скорость вставки 100 целых чисел в карту.
func BenchmarkInsertInterfaceMap100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXInterfaceMap(100, b)
	}
}

func insertXInterfaceMap(x int, b *testing.B) {
	// Инициализируем Map и вставляем X элементов
	testmap := make(map[interface{}]int, 0)
	// Сбрасываем таймер после инициализации Map
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// Вставляем значение I в ключ I.
		testmap[i] = i
	}
}
