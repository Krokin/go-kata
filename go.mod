module gitlab.com/Krokin/go-kata

go 1.19

require (
	github.com/mattn/go-sqlite3 v1.14.16
	gitlab.com/Krokin/greet v0.0.0-20221226234428-d502e0b3409e
)

require (
	github.com/brianvoe/gofakeit/v6 v6.20.1
	github.com/json-iterator/go v1.1.12
	github.com/stretchr/testify v1.3.0
	golang.org/x/sync v0.1.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
