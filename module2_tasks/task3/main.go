package main

import (
	"fmt"
	"sort"
)

func main() {
	nums := []int{4, 4, 1, 1, 1, 2, 2, 3}
	k := 4

	res := KMostRepeat(nums, k)

	fmt.Println(res) // expects [1, 2, 4, 3]
}

type CounterValue struct {
	counter int
	value   int
}

func KMostRepeat(input []int, k int) []int {
	valuesCounter := make(map[int]CounterValue)
	var valCount CounterValue
	for _, v := range input {
		if _, ok := valuesCounter[v]; !ok {
			valuesCounter[v] = CounterValue{1, v}
		} else {
			valCount = valuesCounter[v]
			valCount.counter++
			valuesCounter[v] = valCount
		}
	}
	var listVal []CounterValue
	for _, v := range valuesCounter {
		listVal = append(listVal, v)
	}
	sort.SliceStable(listVal, func(i, j int) bool {
		if listVal[i].counter == listVal[j].counter {
			return false
		}
		return listVal[i].counter > listVal[j].counter
	})
	res := []int{}
	for i := 0; i < k && i < len(listVal); i++ {
		res = append(res, listVal[i].value)
	}
	return res
}
