Задача 3.

Дан массив целых чисел и целое число k. Найти k наиболее часто
встречающихся элементов.
Пример:

Ввод: nums = [4,4,1,1,1,2,2,3], k = 4

Вывод: [1, 2, 4, 3]