package main

import (
	"flag"
	"fmt"
	"io/fs"
	"log"
	"path/filepath"
)

func main() {
	root := flag.String("root path", "/home", "Path for search")
	flag.Parse()
	var fileName string
	fmt.Println("Imput filename")
	fmt.Scan(&fileName)
	err := filepath.WalkDir(*root, func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			return nil
		}
		match, erro := filepath.Match(fileName, d.Name())
		if erro != nil {
			return erro
		}
		if match || d.Name() == fileName {
			fmt.Println(path)
		}
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
}
