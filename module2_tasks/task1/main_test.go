package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncode(t *testing.T) {
	type testCase struct {
		data     string
		expected string
	}
	testCases := []testCase{{
		data:     "AAAABBBCCXYZDDDDEEEFFFAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBB",
		expected: "A4B3C2XYZD4E3F3A6B28",
	}, {
		data:     "aSSSlllccc",
		expected: "aS3l3c3",
	}, {
		data:     "aaaaSSSSSCCCCC",
		expected: "a4S5C5",
	}, {
		data:     "wfaasf2",
		expected: "",
	}, {
		data:     "",
		expected: "",
	}}
	for _, test := range testCases {
		t.Run("EncodeTests", func(t *testing.T) {
			if got := Encode(test.data); got != test.expected {
				t.Errorf("Encode() = %v, expected %v", got, test.expected)
			}
		})
	}
}

func TestDecode(t *testing.T) {
	type testCase struct {
		data     string
		expected string
	}
	testCases := []testCase{{
		data:     "A4B3C2XYZD4E3F3A6B28",
		expected: "AAAABBBCCXYZDDDDEEEFFFAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBB",
	}, {
		data:     "aS3l3c3",
		expected: "aSSSlllccc",
	}, {
		data:     "a4S5C5",
		expected: "aaaaSSSSSCCCCC",
	}, {
		data:     "0wfaasf",
		expected: "",
	}, {
		data:     "SKA01",
		expected: "",
	}}
	for _, test := range testCases {
		t.Run("DecodeTest", func(t *testing.T) {
			res := Decode(test.data)
			assert.Equalf(t, test.expected, res, "Decode() = %s, expected = %s", res, test.expected)
		})
	}
}
