Задача 1. 

Run length encoding. Решить самостоятельно

Input
AAAABBBCCXYZDDDDEEEFFFAAAAAABBBBBBBBBBBBBBBBBBBBB
BBBBBBB
Output
A4B3C2XYZD4E3F3A6B28

Реализовать Encode(string) string и Decode(string) string

Покрыть тестами и бенчмарками. В тесте не менее 3 кейсов.
