package main

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

func validDataEncode(data string) error {
	if data == "" {
		return errors.New("empty data")
	}
	for _, char := range data {
		if char >= '0' && char <= '9' {
			return errors.New("not valid data")
		}
	}
	return nil
}

func validDataDecode(data string) error {
	if data == "" {
		return errors.New("empty data")
	}
	if data[0] >= '0' && data[0] <= '9' {
		return errors.New("not valid data")
	}
	var oldChar rune
	for _, char := range data {
		if (oldChar < '0' || oldChar > '9') && char == '0' {
			return errors.New("not valid data")
		}
		oldChar = char
	}
	return nil
}

func resEncode(count int, charOld string) string {
	res := ""
	if count == 1 {
		res += charOld
	} else {
		res += charOld + strconv.Itoa(count)
	}
	return res
}

func Encode(data string) string {
	if err := validDataEncode(data); err != nil {
		fmt.Println(err)
		return ""
	}
	var (
		charOld rune
		res     string
	)
	count := 1
	for i, char := range data {
		if char == charOld {
			count++
		} else if i != 0 {
			res += resEncode(count, string(charOld))
			count = 1
		}
		charOld = char
	}
	return res + resEncode(count, string(charOld))
}

func Decode(data string) string {
	if err := validDataDecode(data); err != nil {
		fmt.Println(err)
		return ""
	}
	var (
		charOld rune
		res     string
	)

	reg := regexp.MustCompile(`\d+`)
	nums := reg.FindAllString(data, -1)
	pos := 0
	for _, char := range data {
		if char < '0' || char > '9' {
			res += string(char)
		} else if (char >= '0' && char <= '9') && (charOld < '0' || charOld > '9') {
			p, _ := strconv.Atoi(nums[pos])
			for i := 0; i < p-1; i++ {
				res += string(charOld)
			}
			pos++
		}
		charOld = char
	}
	return res
}
