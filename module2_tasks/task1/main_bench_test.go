package main

import "testing"

var (
	listDataEncode = []string{"AAAABBBCCXYZDDDDEEEFFFAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBBB",
		"asasfqwAsdssddaasdaaASADAJAJJJSJDJSJDJDJNNNCNCNCNCNAAC",
		"MCMCMCMCMAMcmmcmzxmcmzxmcaosnawiegnoqwevwmvmxmvzmxvksdvspo"}
	listDataDecode = []string{"A5B8C8asvwgweK9", "K10avnaj5jak9ss2", "u4y1q3w4e5r6t7y8u9i10"}
)

func BenchmarkDecode(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for _, data := range listDataDecode {
			Decode(data)
		}
	}
}

func BenchmarkEncode(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for _, data := range listDataEncode {
			Encode(data)
		}
	}
}
