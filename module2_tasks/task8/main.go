package main

import (
	"flag"
	"fmt"
	jsoniter "github.com/json-iterator/go"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Vacancie struct {
	URL    string `json:"url"`
	Salary int    `json:"salary,omitempty"`
}

func compileRegs(word string) map[string]*regexp.Regexp {
	res := make(map[string]*regexp.Regexp, 7)
	res["start"] = regexp.MustCompile(`class="basic-section"`)
	res["end"] = regexp.MustCompile("content-wrapper__sidebar content-wrapper__sidebar--right")
	res["notPage"] = regexp.MustCompile("Поиск не дал результатов")
	res["salary"] = regexp.MustCompile("basic-salary basic-salary--appearance-vacancy-header")
	res["money"] = regexp.MustCompile(`([0-9]+(\s|\d)+[0-9]+)`)
	res["vacID"] = regexp.MustCompile("/vacancies/[0-9]+")
	res["word"] = regexp.MustCompile(`(?i)(\s|^|[^a-zA-Zа-яА-Я0-9_])` + word + `(\s|$|[^a-zA-Zа-яА-Я0-9_])`)
	return res
}

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) != 1 || args[0] == "" {
		log.Fatal("Invalid argument")
	}
	fmt.Println("Parse now")
	ch := make(chan []byte, 100)
	chData := make(chan Vacancie, 100)
	regs := compileRegs(args[0])
	go vacancies(ch, regs)
	go handleVac(ch, chData, "", regs)
	buildJSON(chData)
}

func validPageVacancies(rawData []byte, regs map[string]*regexp.Regexp) bool {
	return !regs["notPage"].Match(rawData)
}

func vacancies(ch chan []byte, regs map[string]*regexp.Regexp) {
	reg := regs["vacID"]
	listVacancies := make(map[string]string)
	for i := 1; ; i++ {
		URL := fmt.Sprintf("https://career.habr.com/vacancies?page=%d&type=all", i)
		r, err := http.Get(URL)
		if err != nil {
			log.Fatal(err)
		}
		rawData, err := io.ReadAll(r.Body)
		if !validPageVacancies(rawData, regs) {
			r.Body.Close()
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		r.Body.Close()
		for _, d := range reg.FindAll(rawData, -1) {
			if _, ok := listVacancies[string(d)]; !ok {
				listVacancies[string(d)] = string(d)
				ch <- d
			}
		}
	}
	close(ch)
}

func handleVac(ch <-chan []byte, data chan<- Vacancie, word string, regs map[string]*regexp.Regexp) {
	regWord := regs["word"]
	poolGorotines := make(chan struct{}, 10)
	for u := range ch {
		poolGorotines <- struct{}{}
		URL := u
		go func(URL string) {
			URL = "https://career.habr.com" + string(URL)
			r, err := http.Get(URL)
			if err != nil {
				log.Fatal(err)
			}
			defer r.Body.Close()
			if r.StatusCode != http.StatusOK {
				fmt.Printf("Received non-200 status code: %d\n", r.StatusCode)
			} else {
				w, _ := io.ReadAll(r.Body)
				leftPos := regs["start"].FindIndex(w)
				rightPos := regs["end"].FindIndex(w)
				w = w[leftPos[0]:rightPos[0]]
				if regWord.Match(w) {
					data <- Vacancie{URL, findSalary(w, regs)}
				}
			}
			<-poolGorotines
		}(string(URL))
	}
	go func() {
		for {
			if len(poolGorotines) == 0 {
				close(data)
				break
			}
		}
	}()
}

func findSalary(data []byte, regs map[string]*regexp.Regexp) int {
	if !regs["salary"].Match(data) {
		return 0
	}
	pos := regs["salary"].FindIndex(data)
	if len(pos) > 0 {
		data = data[pos[0] : pos[0]+100]
	}
	sal := regs["money"].FindAll(data, 2)
	res := 0
	switch len(sal) {
	case 1:
		sal, err := strconv.Atoi(strings.ReplaceAll(string(sal[0]), " ", ""))
		if err != nil {
			log.Fatal(err)
		}
		res = sal
	case 2:
		min, err := strconv.Atoi(strings.ReplaceAll(string(sal[0]), " ", ""))
		if err != nil {
			log.Fatal(err)
		}
		max, err := strconv.Atoi(strings.ReplaceAll(string(sal[1]), " ", ""))
		if err != nil {
			log.Fatal(err)
		}
		res = (min + max) / 2
	}
	return res
}

func buildJSON(data chan Vacancie) {
	medianSalary := 0
	count := 0
	listVacancies := make([]Vacancie, 0, 25)
	for v := range data {
		if v.Salary != 0 && v.Salary > 10000 {
			medianSalary += v.Salary
			count++
		}
		listVacancies = append(listVacancies, v)
	}
	if len(listVacancies) > 0 {
		res, err := jsoniter.MarshalIndent(listVacancies, "", " ")
		if err != nil {
			log.Fatal(err)
		}
		f, err := os.Create("vacansies.json")
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		_, err = f.Write(res)
		if err != nil {
			log.Fatal(err)
		}
	}
	if count != 0 {
		fmt.Println("Median salary: ", medianSalary/count)
	}
}
