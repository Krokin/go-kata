package main

import (
	"flag"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type Files struct {
	name     string
	levelDep int
	last     bool
}

func main() {
	maxDepth := flag.Int("L", 1000, "Max depth")
	flag.Parse()
	path := flag.Args()
	if len(path) > 1 || *maxDepth < 0 {
		log.Fatal("invalid parse args")
	}
	root := "/home"
	if len(path) == 1 {
		if _, err := os.Stat(path[0]); os.IsNotExist(err) {
			log.Fatal(err)
		}
		root = path[0]
	}
	levelsPrint := make([]Files, 0, 100)
	dir, files := 0, 0
	err := filepath.WalkDir(root, func(path string, d fs.DirEntry, err error) error {
		if path == root {
			return nil
		} else if err != nil {
			log.Fatal(err)
		}
		if d.IsDir() {
			dir++
		} else {
			files++
		}
		path = strings.TrimPrefix(path, root)
		countDepth := strings.Count(path, string(os.PathSeparator))
		levelsPrint = append(levelsPrint, Files{d.Name(), countDepth, false})
		if d.IsDir() && strings.Count(path, string(os.PathSeparator)) >= (*maxDepth)-1 {
			return fs.SkipDir
		}
		return nil
	})
	checkLast(levelsPrint)
	fmt.Println(root)
	printTree(*maxDepth, levelsPrint)
	fmt.Printf("\n%d directories, %d files\n", dir, files)
	if err != nil {
		log.Fatal(err)
	}
}

func checkLast(levelsPrint []Files) {
	var prevFile = Files{}
	list := make(map[int]*Files)
	for i := range levelsPrint {
		if levelsPrint[i].levelDep >= prevFile.levelDep {
			list[levelsPrint[i].levelDep] = &levelsPrint[i]
		} else {
			for _, v := range list {
				if levelsPrint[i].levelDep < v.levelDep {
					v.last = true
				}
			}
			list[levelsPrint[i].levelDep] = &levelsPrint[i]
		}
		prevFile = levelsPrint[i]
	}
	for _, v := range list {
		(*v).last = true
	}
}

func printTree(maxLvl int, levelsPrint []Files) {
	sample := make([]string, maxLvl+2)
	for _, v := range levelsPrint {
		for i := 0; i < v.levelDep; i++ {
			fmt.Print(sample[i])
		}
		fmt.Println("|___", v.name)
		if v.last {
			sample[v.levelDep] = "    "
		} else {
			sample[v.levelDep] = "|   "
		}
	}
}
