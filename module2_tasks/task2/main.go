package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

type User struct {
	Name        string
	OrdersCount int
}

func UsersGenerated(num int) []User {
	list := make([]User, num)
	nameList := []string{"Alex", "Jora", "Mikhail", "Igor", "Caleopa", "Ludovil", "Vlad", "Nikolay", "Dima", "Petya", "Alina",
		"Alexandr", "Anna", "Patricio", "Alena", "Jon", "Viktor", "Oleg", "Jessika", "Max", "Artur", "Luna"}
	for i := 0; i < num; i++ {
		rand.Seed(time.Now().UnixNano() + int64(i))
		list[i].OrdersCount = rand.Intn(100000)
		list[i].Name = nameList[rand.Intn(len(nameList))]
	}
	return list
}

func main() {
	chunk1 := []User{
		{
			Name:        "Alexey",
			OrdersCount: 5,
		},
		{
			Name:        "Nikolay",
			OrdersCount: 34,
		},
		{
			Name:        "Patricio",
			OrdersCount: 75,
		},
		{
			Name:        "Igor",
			OrdersCount: 55,
		},
		{
			Name:        "Alexandr",
			OrdersCount: 89,
		},
		{
			Name:        "Alina",
			OrdersCount: 13,
		},
	}

	chunk2 := []User{
		{
			Name:        "Andrey",
			OrdersCount: 1,
		},
		{
			Name:        "Mikhail",
			OrdersCount: 144,
		},
		{
			Name:        "Ludovik",
			OrdersCount: 233,
		},
		{
			Name:        "Max",
			OrdersCount: 2,
		},
		{
			Name:        "Alexandr",
			OrdersCount: 2,
		},
		{
			Name:        "Alena",
			OrdersCount: 5,
		},
	}

	chunk1 = QuickSort(chunk1)
	fmt.Println(chunk1)
	chunk2 = QuickSort(chunk2)
	fmt.Println(chunk2)
	res := MergeUsers(chunk1, chunk2)
	fmt.Println(res)
}

func QuickSort(s []User) []User {
	if len(s) < 2 {
		return s
	}
	p := s[len(s)/2].OrdersCount
	i, j := 0, len(s)-1
	for i <= j {
		for ; s[i].OrdersCount < p; i++ {
		}
		for ; s[j].OrdersCount > p; j-- {
		}
		if i <= j {
			s[i], s[j] = s[j], s[i]
			i++
			j--
		}
	}
	if i < len(s)-1 {
		QuickSort(s[i:])
	}
	if j > 0 {
		QuickSort(s[:i])
	}
	return s
}

func MergeUsers(chunk1 []User, chunk2 []User) []User {
	// смержить 2 отстортированных массива по возрастанию поля OrdersCount
	// используя 2 указателя p1 и p2
	var p1, p2 int
	p1, p2 = 0, 0
	var buffValues = make([]User, 0, len(chunk2)+len(chunk1))
	for p1 < len(chunk1) && p2 < len(chunk2) {
		if chunk1[p1].OrdersCount < chunk2[p2].OrdersCount {
			buffValues = append(buffValues, chunk1[p1])
			p1++
		} else {
			buffValues = append(buffValues, chunk2[p2])
			p2++
		}
	}
	if p1 != len(chunk1) {
		buffValues = append(buffValues, chunk1[p1:]...)
	}
	if p2 != len(chunk2) {
		buffValues = append(buffValues, chunk2[p2:]...)
	}
	count := 0
	for i := 0; i < len(chunk1); i++ {
		chunk1[i] = buffValues[count]
		count++
	}
	for i := 0; i < len(buffValues)-len(chunk1); i++ {
		chunk2[i] = buffValues[count]
		count++
	}
	return append(chunk1, chunk2...)
}

func Sort(input []int) []int {
	l := len(input)
	if l < 2 {
		return input
	}
	less := make([]int, 0)
	bigger := make([]int, 0)
	pivot := input[0]
	for _, v := range input[1:] {
		if v > pivot {
			bigger = append(bigger, v)
		} else {
			less = append(less, v)
		}
	}
	input = append(Sort(less), pivot)
	input = append(input, Sort(bigger)...)
	return input
}

func SortGo(input []int) []int {
	sort.Slice(input, func(i, j int) bool {
		return input[i] < input[j]
	})
	return input
}

func QuickSort1(s []int) []int {
	p := s[len(s)/2]
	i, j := 0, len(s)-1
	for i <= j {
		for ; s[i] < p; i++ {
		}
		for ; s[j] > p; j-- {
		}
		if i <= j {
			s[i], s[j] = s[j], s[i]
			i++
			j--
		}
	}
	if i < len(s)-1 {
		QuickSort1(s[i:])
	}
	if j > 0 {
		QuickSort1(s[:i])
	}
	return s
}
