package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestQuickSort(t *testing.T) {
	type testCase struct {
		data     []User
		expected []User
	}
	tests := []testCase{{
		data:     []User{{"Nikolay", 5}, {"Max", -123}},
		expected: []User{{"Max", -123}, {"Nikolay", 5}},
	}, {
		data:     []User{{}},
		expected: []User{{}},
	}, {
		data:     []User{{"Nikolay", 5}, {"Max", -123}, {"Maxim", 123}, {"Mark", 0}, {"Masha", 229}},
		expected: []User{{"Max", -123}, {"Mark", 0}, {"Nikolay", 5}, {"Maxim", 123}, {"Masha", 229}},
	}, {
		data:     nil,
		expected: nil,
	}}
	for _, test := range tests {
		t.Run("QuickSortTests", func(t *testing.T) {
			if got := QuickSort(test.data); !assert.Equal(t, got, test.expected) {
				t.Errorf("QuickSort() = %v, expected %v", got, test.expected)
			}
		})
	}
}

func TestMergeUsers(t *testing.T) {
	type testCase struct {
		data     [][]User
		expected []User
	}
	tests := []testCase{{
		data:     [][]User{{{"Nikolay", 5}}, {{"Max", -123}}},
		expected: []User{{"Max", -123}, {"Nikolay", 5}},
	}, {
		data:     [][]User{{{"Nikolay", 5}}, {}},
		expected: []User{{"Nikolay", 5}},
	}, {
		data:     [][]User{{}, {{"Nikolay", 5}}},
		expected: []User{{"Nikolay", 5}},
	}, {
		data:     [][]User{{}, {}},
		expected: []User{},
	}, {
		data:     [][]User{{{"Max", -123}, {"Nikolay", 5}, {"Maxim", 123}}, {{"Mark", 0}, {"Masha", 229}, {"Dimon", 1100}}},
		expected: []User{{"Max", -123}, {"Mark", 0}, {"Nikolay", 5}, {"Maxim", 123}, {"Masha", 229}, {"Dimon", 1100}},
	}}
	for _, test := range tests {
		t.Run("MergeUsersTests", func(t *testing.T) {
			if got := MergeUsers(test.data[0], test.data[1]); !assert.Equal(t, got, test.expected) {
				t.Errorf("MergeUsers() = %v, expected %v", got, test.expected)
			}
		})
	}
}

func TestUsersGenerated(t *testing.T) {
	type testCase struct {
		data     int
		expected int
	}
	tests := []testCase{{1, 1}, {10, 10}, {100, 100}}
	for _, test := range tests {
		t.Run("TestUsersGenerated", func(t *testing.T) {
			if got := UsersGenerated(test.data); len(got) != test.expected {
				t.Errorf("TestUsersGenerated() = len:%v, expected %v", got, test.expected)
			}
		})
	}
}
