package main

import (
	"bufio"
	"fmt"
	"os"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"sync"
)

type Data struct {
	IP         string
	CountCalls int
}

func da() {
	ch1 := make(chan string, 500)
	go gopherRead(ch1)
	ch2 := make(chan Data, 500)
	wg := &sync.WaitGroup{}
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go gopherBuild(ch1, ch2, wg)
	}
	go func() {
		wg.Wait()
		close(ch2)
	}()
	finish := make(chan struct{})
	go gopherSort(ch2, finish)
	<-finish

}
func main() {
	da()
}

func gopherRead(ch chan string) {
	defer close(ch)
	file, err := os.Open("./module2_tasks/task5/access.log")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()
	s := bufio.NewScanner(file)
	s.Split(bufio.ScanLines)
	for s.Scan() {
		ch <- s.Text()
	}
}

func gopherBuild(input chan string, output chan Data, wg *sync.WaitGroup) {
	for str := range input {
		data := strings.Split(str, " ")
		count, err := strconv.Atoi(data[8])
		if err != nil {
			fmt.Println(err)
			continue
		}
		output <- Data{
			data[0],
			count,
		}
	}
	wg.Done()
}

func gopherSort(input chan Data, finish chan struct{}) {
	var listData = make([]Data, 0, 100000)
	for data := range input {
		listData = append(listData, data)
	}
	sort.Slice(listData, func(i, j int) bool {
		return listData[i].CountCalls > listData[j].CountCalls
	})
	gopherOutput(listData[:15])
	finish <- struct{}{}
}

func gopherOutput(list []Data) {
	for i, v := range list {
		_, _ = i, v
		fmt.Printf("%d. IP: %s, calls: %d\n", i+1, v.IP, v.CountCalls)
	}
}
