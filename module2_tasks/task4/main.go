package main

import (
	"errors"
	"fmt"
	"strings"
)

func main() {
	res := titleToNumber("AB")
	fmt.Println(res) // 28
}

var vals = map[byte]int{'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7,
	'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14,
	'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21,
	'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26}

func validData(data string) error {
	if len(data) > 3 || len(data) < 1 {
		return errors.New("not valid")
	}
	for _, v := range data {
		if v < 65 || v > 90 {
			return errors.New("not valid")
		}
	}
	return nil
}

func titleToNumber(columnTitle string) int {
	columnTitle = strings.ToUpper(columnTitle)
	if err := validData(columnTitle); err != nil {
		fmt.Println(err)
		return -1
	}
	var res int
	switch len(columnTitle) {
	case 1:
		res = vals[columnTitle[0]]
	case 2:
		res = (26 * vals[columnTitle[0]]) + vals[columnTitle[1]]
	case 3:
		res = (676 * vals[columnTitle[0]]) + (26 * vals[columnTitle[1]]) + vals[columnTitle[2]]
	}
	if res > 16384 {
		fmt.Println("error: max value")
		res = -1
	}
	return res
}
