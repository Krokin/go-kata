Получить номер колонки в excel по буквенной записи:

A -> 1

B -> 2

C -> 3

...

Z -> 26

AA -> 27

AB -> 28

...

Example 1:

Input: columnTitle = "A"

Output: 1

Example 2:

Input: columnTitle = "AB"

Output: 28

Example 3:

Input: columnTitle = "ZY"

Output: 701