package main

import (
	"math"
)

func main() {
	minimumAverageDifference([]int{0})
}

func minimumAverageDifference(nums []int) int {
	sumAll := 0
	for _, v := range nums {
		sumAll += v
	}
	sumStart := 0
	index := 0
	lenR := len(nums)
	res := math.MaxInt64
	for i, n := range nums {
		sumStart += n
		sumAll -= n
		if lenR-(i+1) < 1 {
			lenR++
		}
		resAv := int(math.Abs(float64(sumStart/(i+1) - sumAll/(lenR-(i+1)))))
		if resAv < res {
			index = i
			res = resAv
		}
	}
	return index
}
