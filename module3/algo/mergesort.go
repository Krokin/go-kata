package algo

func mergeSort(data []int) []int {
	if len(data) < 2 {
		return data
	}
	first := mergeSort(data[:len(data)/2])
	second := mergeSort(data[len(data)/2:])

	return merge(first, second)
}

func merge(first, second []int) []int {
	final := []int{}
	i, j := 0, 0
	for i < len(first) && j < len(second) {
		if first[i] < second[j] {
			final = append(final, first[i])
			i++
		} else {
			final = append(final, second[j])
			j++
		}
	}
	for ; i < len(first); i++ {
		final = append(final, first[i])
	}

	for ; j < len(second); j++ {
		final = append(final, second[j])
	}
	return final
}
