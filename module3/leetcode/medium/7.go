package main

func balanceBST(root *TreeNode) *TreeNode {
	res := []*TreeNode{}
	var c func(*TreeNode)
	c = func(node *TreeNode) {
		if node == nil {
			return
		}
		c(node.Left)
		res = append(res, node)
		c(node.Right)
	}
	c(root)
	var t func(s []*TreeNode) *TreeNode
	t = func(s []*TreeNode) *TreeNode {
		if len(s) < 1 {
			return nil
		}
		n := len(s) / 2
		s[n].Left = t(s[:n])
		s[n].Right = t(s[n+1:])
		return s[n]
	}
	return t(res)
}
