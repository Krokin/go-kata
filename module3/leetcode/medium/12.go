package main

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	res := list1
	var start, startL2 *ListNode
	startL2 = list2
	for list2.Next != nil {
		list2 = list2.Next
	}
	for i := 0; list1 != nil; i++ {
		if i == a-1 {
			start = list1
		}
		if i == b+1 {
			list2.Next = list1
			break
		}
		list1 = list1.Next
	}
	start.Next = startL2
	return res
}
