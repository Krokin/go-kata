package main

func pairSum(head *ListNode) int {
	m := make([]int, 0, 1000)
	st := head
	for st != nil {
		m = append(m, st.Val)
		st = st.Next
	}
	max := 0
	for i, j := 0, len(m)-1; i < len(m)/2; i++ {
		num := m[i] + m[j]
		if num > max {
			max = num
		}
		j--
	}
	return max
}
