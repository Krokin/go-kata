package main

func groupThePeople(groupSizes []int) [][]int {
	m := make(map[int][]int)
	res := [][]int{}
	for i, v := range groupSizes {
		m[v] = append(m[v], i)
	}
	for k, v := range m {
		for i := 0; i < len(v); {
			l := make([]int, 0, k)
			for j := k; j > 0; j-- {
				l = append(l, v[i])
				i++
			}
			res = append(res, l)
		}
	}
	return res
}
