package main

func xorQueries(arr []int, queries [][]int) []int {
	xor := func(n []int) int {
		res := 0
		for _, v := range n {
			res ^= v
		}
		return res
	}
	res := make([]int, 0, len(queries))
	for _, s := range queries {
		res = append(res, xor(arr[s[0]:s[1]+1]))
	}
	return res
}
