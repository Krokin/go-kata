package main

func averageOfSubtree(root *TreeNode) int {
	res := 0
	sum(root, &res)
	return res
}

func sum(root *TreeNode, res *int) (int, int) {
	count := 1
	sumNodes := root.Val
	if root.Right == nil && root.Left == nil {
		*res++
		return sumNodes, count
	}
	if root.Right != nil {
		s, c := sum(root.Right, res)
		count += c
		sumNodes += s
	}
	if root.Left != nil {
		s, c := sum(root.Left, res)
		count += c
		sumNodes += s
	}
	if sumNodes/count == root.Val {
		*res++
	}
	return sumNodes, count
}
