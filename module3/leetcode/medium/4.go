package main

func bstToGst(root *TreeNode) *TreeNode {
	root.Val += recurs(root.Right, 0)
	recurs(root.Left, root.Val)
	return root
}

func recurs(root *TreeNode, sum int) int {
	if root == nil {
		return 0
	}
	if root.Right == nil && root.Left == nil {
		root.Val += sum
		return root.Val
	}
	if root.Right != nil {
		root.Val += recurs(root.Right, sum)
	} else {
		root.Val += sum
	}
	res := root.Val
	if root.Left != nil {
		res = recurs(root.Left, root.Val)
	}
	return res
}
