package main

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	m := make(map[int]struct{}, n)
	for _, s := range edges {
		m[s[1]] = struct{}{}
	}
	res := []int{}
	resM := make(map[int]struct{}, n)
	for _, s := range edges {
		if _, ok := resM[s[0]]; ok {
			continue
		}
		if _, ok := m[s[0]]; !ok {
			resM[s[0]] = struct{}{}
			res = append(res, s[0])
		}
	}
	return res
}
