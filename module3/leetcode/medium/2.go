package main

import (
	"sort"
)

func sortTheStudents(score [][]int, k int) [][]int {
	res := score
	sort.Slice(res, func(i, j int) bool {
		return res[i][k] > res[j][k]
	})
	return res
}
