package main

func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) < 1 {
		return nil
	}
	max := 0
	index := 0
	for i := 0; i < len(nums); i++ {
		if nums[i] > max {
			max = nums[i]
			index = i
		}
	}
	return &TreeNode{max, constructMaximumBinaryTree(nums[:index]), constructMaximumBinaryTree(nums[index+1:])}
}
