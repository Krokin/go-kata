package main

import "strconv"

func minPartitions(n string) int {
	var res int32
	for _, v := range n {
		if v > res {
			res = v
		}
	}
	ans, _ := strconv.Atoi(string(res))
	return ans
}
