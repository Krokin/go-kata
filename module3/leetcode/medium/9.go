package main

import (
	"sort"
)

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	f := func(n []int) bool {
		buff := make([]int, len(n))
		copy(buff, n)
		sort.Ints(buff)
		res := true
		arifmetic := buff[1] - buff[0]
		for i := 0; i < len(buff)-1; i++ {
			if buff[i+1]-buff[i] != arifmetic {
				res = false
				break
			}
		}
		return res
	}
	ans := []bool{}
	for j := 0; j < len(l); j++ {
		ans = append(ans, f(nums[l[j]:r[j]+1]))
	}
	return ans
}
