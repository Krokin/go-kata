package main

import "math"

func countPoints(points [][]int, queries [][]int) []int {
	res := []int{}
	for _, c := range queries {
		ans := 0
		for _, p := range points {
			if math.Sqrt(math.Pow(float64(p[0]-c[0]), 2)+math.Pow(float64(p[1]-c[1]), 2)) <= float64(c[2]) {
				ans++
			}
		}
		res = append(res, ans)
	}
	return res
}
