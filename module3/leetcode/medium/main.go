package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	fmt.Println("1: ", deepestLeavesSum(&TreeNode{1, &TreeNode{2, &TreeNode{4, &TreeNode{7, nil, nil}, nil}, &TreeNode{5, nil, nil}}, &TreeNode{3, nil, &TreeNode{6, nil, &TreeNode{8, nil, nil}}}}))
	fmt.Println("2: ", sortTheStudents([][]int{{10, 6, 9, 1}, {7, 5, 11, 2}, {4, 8, 3, 15}}, 2))
	t := &ListNode{0, &ListNode{3, &ListNode{1, &ListNode{0, &ListNode{4, &ListNode{0, nil}}}}}}
	fmt.Println("3: ", mergeNodes(t))
	fmt.Println("4: ", bstToGst(&TreeNode{4, &TreeNode{1, &TreeNode{0, nil, nil}, &TreeNode{2, nil, &TreeNode{3, nil, nil}}}, &TreeNode{6, &TreeNode{5, nil, nil}, &TreeNode{7, nil, &TreeNode{8, nil, nil}}}}))
	t1 := &ListNode{5, &ListNode{4, &ListNode{2, &ListNode{1, nil}}}}
	fmt.Println("5: ", pairSum(t1))
	fmt.Println("6 ", constructMaximumBinaryTree([]int{3, 2, 1, 6, 0, 5}))
	fmt.Println("7: ", balanceBST(&TreeNode{4, &TreeNode{1, &TreeNode{0, nil, nil}, &TreeNode{2, nil, &TreeNode{3, nil, nil}}}, &TreeNode{6, &TreeNode{5, nil, nil}, &TreeNode{7, nil, &TreeNode{8, nil, nil}}}}))
	fmt.Println("8: ", findSmallestSetOfVertices(6, [][]int{{0, 1}, {0, 2}, {2, 5}, {3, 4}, {4, 2}}))
	fmt.Println("9: ", checkArithmeticSubarrays([]int{4, 6, 5, 9, 3, 7}, []int{0, 0, 2}, []int{2, 3, 5}))
	fmt.Println("10: ", numTilePossibilities("AAABBC"))
	fmt.Println("11: ", removeLeafNodes(&TreeNode{1, &TreeNode{2, nil, nil}, nil}, 2).Val)
	fmt.Println("12: ", mergeInBetween(&ListNode{0, &ListNode{1, &ListNode{2, &ListNode{3, &ListNode{4, &ListNode{5, nil}}}}}}, 3, 4, &ListNode{100, &ListNode{101, &ListNode{102, nil}}}))
	fmt.Println("13: ", maxSum([][]int{{6, 2, 1, 3}, {4, 2, 1, 5}, {9, 2, 8, 7}, {4, 1, 2, 9}}))
	fmt.Println("14: ", xorQueries([]int{1, 3, 4, 8}, [][]int{{0, 1}, {1, 2}, {0, 3}, {3, 3}}))
	fmt.Println("15: ", minPartitions("82734"))
	fmt.Println("16: ", Constructor([][]int{{1, 3}, {2, 5}}))
	fmt.Println("17: ", countPoints([][]int{{1, 3}, {3, 3}, {5, 3}, {2, 2}}, [][]int{{2, 3, 1}, {4, 3, 1}, {1, 1, 2}}))
	fmt.Println("18: ", groupThePeople([]int{3, 3, 3, 3, 3, 1, 3}))
	fmt.Println("19: ", averageOfSubtree(&TreeNode{4, &TreeNode{1, &TreeNode{0, nil, nil}, &TreeNode{2, nil, &TreeNode{3, nil, nil}}}, &TreeNode{6, &TreeNode{5, nil, nil}, &TreeNode{7, nil, &TreeNode{8, nil, nil}}}}))
	fmt.Println("20: ", processQueries([]int{3, 1, 2, 1}, 5))
}
