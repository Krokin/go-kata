package main

func processQueries(queries []int, m int) []int {
	p := make([]int, m)
	for i := 1; i <= m; i++ {
		p[i-1] = i
	}
	res := []int{}
	for _, v := range queries {
		for i := 0; i < m; i++ {
			if p[i] == v {
				res = append(res, i)
				buff := append([]int{v}, append(p[:i], p[i+1:]...)...)
				p = buff
			}
		}
	}
	return res
}
