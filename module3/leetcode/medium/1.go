package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	res := 0
	l := []*TreeNode{root}
	for len(l) > 0 {
		len := len(l)
		fmt.Println(len, "Len")
		res = 0
		for i := 0; i < len; i++ {
			node := l[i]
			res += node.Val

			if node.Left != nil {
				l = append(l, node.Left)
			}
			if node.Right != nil {
				l = append(l, node.Right)
			}
		}
		l = l[len:]
	}
	return res
}
