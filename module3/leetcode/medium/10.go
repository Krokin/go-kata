package main

func numTilePossibilities(tiles string) int {
	m := make(map[string]struct{})
	chars := []rune{}
	for _, v := range tiles {
		chars = append(chars, v)
	}
	buff := make([]bool, len(tiles))
	for i := 1; i <= len(tiles); i++ {
		dfs("", i, buff, m, chars)
	}
	return len(m)
}

func dfs(str string, l int, buff []bool, m map[string]struct{}, chars []rune) {
	if len(str) == l {
		m[str] = struct{}{}
		return
	}
	for i := 0; i < len(buff); i++ {
		if !buff[i] {
			buff[i] = true
			dfs(str+string(chars[i]), l, buff, m, chars)
			buff[i] = false
		}
	}
}

// A B C
// AA AB AC BB BC
// AAA AAB AAC ABB ABC BBC
// AAAB AAAC AABB AABC ABBC
// AAABB AAABC AABBC
// AAABBC

// A AA AAA B BB C
