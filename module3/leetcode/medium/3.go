package main

func mergeNodes(head *ListNode) *ListNode {
	sum := 0
	l := &ListNode{}
	res := l
	head = head.Next
	for head.Next != nil {
		if head.Val == 0 {
			l.Val = sum
			l.Next = &ListNode{}
			l = l.Next
			sum = 0
		}
		sum += head.Val
		head = head.Next
	}
	l.Val = sum
	return res
}
