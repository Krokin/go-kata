package main

import (
	"sort"
)

func sortPeople(names []string, heights []int) []string {
	type P struct {
		name   string
		height int
	}
	l := []P{}
	for i, h := range names {
		l = append(l, P{h, heights[i]})
	}
	sort.Slice(l, func(i, j int) bool {
		return l[i].height > l[j].height
	})
	res := []string{}
	for _, i := range l {
		res = append(res, i.name)
	}
	return res
}
