package main

func interpret(command string) string {
	res := ""
	for i := 0; i < len(command); i++ {
		if command[i] == '(' && command[i+1] == ')' {
			res += "o"
			i += 1
		} else if command[i] == '(' {
			res += "al"
			i += 3
		} else {
			res += "G"
		}
	}
	return res
}
