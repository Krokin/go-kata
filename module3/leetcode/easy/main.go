package main

import "fmt"

func main() {
	fmt.Println(tribonacci(37))
	fmt.Println(getConcatenation([]int{2, 3, 4}))
	fmt.Println(convertTemperature(36.6))
	fmt.Println(buildArray([]int{1, 2, 3, 4, 5, 5}))
	t := &TreeNode{2, &TreeNode{1, nil, nil}, &TreeNode{3, nil, nil}}
	t = invertTree(t)
	fmt.Println(t.Left.Val, t.Right.Val)
	fmt.Println(removeDuplicates([]int{0, 0, 1, 1, 1, 2, 2, 3, 3, 4}))
	fmt.Println(defangIPaddr("1.1.1.1"))
	fmt.Println(sum(10, 8))
	fmt.Println(finalValueAfterOperations([]string{"++X", "--X"}))
	fmt.Println(shuffle([]int{1, 2, 3, 4, 5, 6, 7, 8}, 4))
	fmt.Println(runningSum([]int{1, 2, 3, 5, 6, 7}))
	fmt.Println(numIdenticalPairs([]int{1, 2, 3, 1, 1, 3}))
	fmt.Println(numJewelsInStones("aA", "AaacCcascjaia"))
	fmt.Println(maximumWealth([][]int{{2, 3, 4}, {2, 5, 7}}))
	c := Constructor(5, 5, 5)
	_ = c
	fmt.Println(smallestEvenMultiple(10))
	fmt.Println(mostWordsFound([]string{"ASD sad c", "cas qw2 asf 1"}))
	fmt.Println(differenceOfSum([]int{1, 2, 3, 45, 5}))
	fmt.Println(minimumSum(2021))
	fmt.Println(kidsWithCandies([]int{1, 5, 7, 5, 4, 3}, 3))
	fmt.Println(subtractProductAndSum(234))
	fmt.Println(smallerNumbersThanCurrent([]int{8, 1, 2, 2, 3}))
	fmt.Println(interpret("G()()()()(al)"))
	fmt.Println(decode([]int{1, 2, 3}, 1))
	fmt.Println(createTargetArray([]int{4, 2, 4, 3, 2}, []int{0, 0, 1, 3, 1}))
	fmt.Println(decompressRLElist([]int{1, 2, 3, 4}))
	fmt.Println(balancedStringSplit("RLRRRLLRLL"))
	fmt.Println(countDigits(1244))
	fmt.Println(xorOperation(4, 3))
	fmt.Println(countGoodTriplets([]int{3, 0, 1, 1, 9, 7}, 7, 2, 3))
	fmt.Println(sortPeople([]string{"Mary", "John", "Emma"}, []int{180, 165, 170}))
}
