package main

import "fmt"

func differenceOfSum(nums []int) int {
	sum := 0
	sumDelim := 0
	for _, n := range nums {
		sum += n
		for n > 0 {
			sumDelim += n % 10
			fmt.Println(sumDelim)
			n /= 10
		}
	}
	return sum - sumDelim
}
