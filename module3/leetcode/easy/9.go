package main

func finalValueAfterOperations(operations []string) int {
	res := 0
	for _, oper := range operations {
		switch {
		case oper == "--X" || oper == "X--":
			res--
		case oper == "++X" || oper == "X++":
			res++
		}
	}
	return res
}
