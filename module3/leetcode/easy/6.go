package main

// Задача 6 как 3, взял рандомную
// https://leetcode.com/problems/remove-duplicates-from-sorted-array/

func removeDuplicates(nums []int) int {
	m := make(map[int]struct{})
	i := 0
	for j := 0; j < len(nums); j++ {
		if _, ok := m[nums[j]]; !ok {
			m[nums[j]] = struct{}{}
			nums[i] = nums[j]
			i++
		}
	}
	return i
}
