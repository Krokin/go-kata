package main

func smallestEvenMultiple(n int) int {
	res := n
	for {
		if res%n == 0 && res%2 == 0 {
			break
		}
		res += n
	}
	return res
}
