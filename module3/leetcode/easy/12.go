package main

func numIdenticalPairs(nums []int) int {
	res := 0
	for len(nums) > 1 {
		for i := 1; i < len(nums); i++ {
			if nums[0] == nums[i] {
				res++
			}
		}
		nums = nums[1:]
	}
	return res
}
