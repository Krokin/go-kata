package main

func maximumWealth(accounts [][]int) int {
	res := 0
	for _, c := range accounts {
		cash := 0
		for _, m := range c {
			cash += m
		}
		if cash > res {
			res = cash
		}
	}
	return res
}
