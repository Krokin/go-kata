package main

func smallerNumbersThanCurrent(nums []int) []int {
	res := make([]int, 0, len(nums))
	for _, n := range nums {
		count := 0
		for _, m := range nums {
			if m < n {
				count++
			}
		}
		res = append(res, count)
	}
	return res
}
