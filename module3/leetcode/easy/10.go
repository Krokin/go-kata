package main

func shuffle(nums []int, n int) []int {
	l := nums[:n]
	r := nums[n:]
	res := make([]int, 0, len(nums))
	for i := 0; i < n; i++ {
		res = append(res, l[i])
		res = append(res, r[i])
	}
	return res
}
