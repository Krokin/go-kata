package main

func kidsWithCandies(candies []int, extraCandies int) []bool {
	max := 0
	for _, c := range candies {
		if c > max {
			max = c
		}
	}
	res := make([]bool, 0, len(candies))
	var ok bool
	for _, c := range candies {
		if c+extraCandies >= max {
			ok = true
		}
		res = append(res, ok)
		ok = false
	}
	return res
}
