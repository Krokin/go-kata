package main

func subtractProductAndSum(n int) int {
	product := 1
	sum := 0
	for n > 0 {
		num := n % 10
		n /= 10
		product *= num
		sum += num
	}
	return product - sum
}
