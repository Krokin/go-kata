package main

func tribonacci(n int) int {
	l := []int{0, 1, 1}
	if n < 3 {
		return l[n]
	}
	for i := 2; i < n; i++ {
		l = append(l, l[i]+l[i-1]+l[i-2])
	}
	return l[len(l)-1]
}
