package main

import "fmt"

func decode(encoded []int, first int) []int {
	res := []int{first}
	for _, v := range encoded {
		fmt.Println(v, first, v^first)
		res = append(res, v^first)
		first = v ^ first
	}
	return res
}
