package main

import (
	"sort"
)

func minimumSum(num int) int {
	nums := []int{}
	for num > 0 {
		nums = append(nums, num%10)
		num /= 10
	}
	sort.Slice(nums, func(i, j int) bool {
		return nums[i] < nums[j]
	})
	return (10*nums[0] + nums[3]) + (10*nums[1] + nums[2])
}
