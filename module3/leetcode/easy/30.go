package main

import (
	"fmt"
	"math"
)

func countGoodTriplets(arr []int, a int, b int, c int) int {
	res := 0
	for i := 0; i < len(arr)-2; i++ {
		for j := i + 1; j < len(arr)-1; j++ {
			for k := j + 1; k < len(arr); k++ {
				if int(math.Abs(float64(arr[i]-arr[j]))) <= a {
					if int(math.Abs(float64(arr[j]-arr[k]))) <= b {
						if int(math.Abs(float64(arr[i]-arr[k]))) <= c {
							fmt.Println(i, j, k)
							res++
						}
					}

				}
			}
		}
	}
	return res
}
