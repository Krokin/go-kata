package main

func defangIPaddr(address string) string {
	res := []byte{}
	pattern := []byte{'[', '.', ']'}
	for _, b := range address {
		if b == '.' {
			res = append(res, pattern...)
		} else {
			res = append(res, byte(b))
		}
	}
	return string(res)
}
