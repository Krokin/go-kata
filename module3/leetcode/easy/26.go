package main

func decompressRLElist(nums []int) []int {
	res := []int{}
	for i := 0; i < len(nums); i++ {
		n := nums[i+1]
		for j := nums[i]; j > 0; j-- {
			res = append(res, n)
		}
		i++
	}
	return res
}
