package main

type ParkingSystem struct {
	big    int
	medium int
	small  int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	return ParkingSystem{big, medium, small}
}

func (this *ParkingSystem) AddCar(carType int) bool {
	var res bool
	if carType == 1 {
		if this.big > 0 {
			this.big--
			res = true
		}
	} else if carType == 2 {
		if this.medium > 0 {
			this.medium--
			res = true
		}
	} else if carType == 3 {
		if this.small > 0 {
			this.small--
			res = true
		}
	}
	return res
}
