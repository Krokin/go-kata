package main

func runningSum(nums []int) []int {
	res := make([]int, 0, len(nums))
	res = append(res, nums[0])
	for i := 1; i < len(nums); i++ {
		res = append(res, nums[i]+res[len(res)-1])
	}
	return res
}
