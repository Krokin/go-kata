package main

func countDigits(num int) int {
	res := 0
	d := num
	for ; num > 0; num /= 10 {
		if d%(num%10) == 0 {
			res++
		}
	}
	return res
}
