package main

func numJewelsInStones(jewels string, stones string) int {
	res := 0
	for _, j := range jewels {
		for _, s := range stones {
			if j == s {
				res++
			}
		}
	}
	return res
}
