package main

func balancedStringSplit(s string) int {
	res := 0
	r := s[0]
	count := 1
	for i := 1; i < len(s); i++ {
		if r == s[i] {
			count++
		} else {
			count--
		}
		if count == 0 {
			res++
			if i+1 > len(s)-1 {
				break
			}
			r = s[i+1]
			i++
			count = 1
		}
	}
	return res
}
