package main

import "strings"

func mostWordsFound(sentences []string) int {
	res := 0
	for _, s := range sentences {
		l := len(strings.Split(s, " "))
		if res < l {
			res = l
		}
	}
	return res
}
