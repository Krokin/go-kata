package main

func xorOperation(n int, start int) int {
	count := 1
	res := start
	for i := n - 1; i > 0; i-- {
		res ^= start + (2 * count)
		count++
	}
	return res
}
