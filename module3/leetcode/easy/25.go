package main

func createTargetArray(nums []int, index []int) []int {
	res := make([]int, 0, len(nums))
	for j, i := range index {
		res = append(res[:i], append([]int{nums[j]}, res[i:]...)...)
	}
	return res
}
