package main

import (
	"log"

	"gitlab.com/Krokin/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/Krokin/go-kata/module3/clean_architecture/service/repo"

	"gitlab.com/Krokin/go-kata/module3/clean_architecture/service/service"
)

func main() {
	rep := repo.NewFileRepository("test.json")
	todo := service.NewToDoService(rep)
	err := todo.AddTask(&model.Task{
		Id:     1,
		Title:  "",
		Text:   "",
		Status: "",
	})
	if err != nil {
		log.Fatal(err)
	}
}
