package repo

import (
	"encoding/json"
	"errors"
	"io"
	"os"

	"gitlab.com/Krokin/go-kata/module3/clean_architecture/service/model"
)

func NewFileRepository(path string) model.Repository {
	return ToDoRepository{filepath: path}
}

func checkTaskExist(task *model.Task, list []*model.Task) bool {
	for _, t := range list {
		if task.GetID() == t.GetID() {
			return true
		}
	}
	return false
}

type ToDoRepository struct {
	filepath string
}

func (t ToDoRepository) DeleteTask(ID int) error {
	tasks, err := t.ReadAllTask()
	if err != nil {
		return err
	}
	for i, v := range tasks {
		if v.Id == ID {
			tasks = append(tasks[:i], tasks[i+1:]...)
			if err = t.saveTasks(tasks); err != nil {
				return err
			}
			return nil
		}
	}
	return errors.New("not found task")
}

func (t ToDoRepository) UpdateTask(ID int, title, text string) error {
	tasks, err := t.ReadAllTask()
	if err != nil {
		return err
	}
	for _, v := range tasks {
		if v.Id == ID {
			v.TaskUpdate(title, text)
			if err = t.saveTasks(tasks); err != nil {
				return err
			}
			return nil
		}
	}
	return errors.New("not found task")
}

func (t ToDoRepository) CreateTask(task *model.Task) error {
	tasks, err := t.ReadAllTask()
	if err != nil {
		return err
	}
	if checkTaskExist(task, tasks) {
		return errors.New("task with this id exists")
	}
	tasks = append(tasks, task)
	if err = t.saveTasks(tasks); err != nil {
		return err
	}
	return nil
}

func (t ToDoRepository) ReadAllTask() ([]*model.Task, error) {
	f, err := os.Open(t.filepath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var res []*model.Task
	if err = json.NewDecoder(f).Decode(&res); err != nil && err != io.EOF {
		return nil, err
	}
	return res, nil
}

func (t ToDoRepository) CompleteTask(ID int) error {
	tasks, err := t.ReadAllTask()
	if err != nil {
		return err
	}
	for _, v := range tasks {
		if v.Id == ID {
			v.TaskStatusComplete()
			if err = t.saveTasks(tasks); err != nil {
				return err
			}
			return nil
		}
	}
	return errors.New("not found task")
}

func (t ToDoRepository) saveTasks(data []*model.Task) error {
	f, err := os.Create(t.filepath)
	if err != nil {
		return err
	}
	defer f.Close()
	b, err := json.Marshal(data)
	if err != nil {
		return err
	}
	_, err = f.Write(b)
	if err != nil {
		return err
	}
	return nil
}
