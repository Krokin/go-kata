package service

import (
	"gitlab.com/Krokin/go-kata/module3/clean_architecture/service/model"
)

type toDoService struct {
	UseCaseDelete
	UseCaseAdd
	UseCaseComplete
	UseCaseGetList
	UseCaseUpdate
}

func NewToDoService(r model.Repository) model.ToDoService {
	return toDoService{
		UseCaseDelete{r},
		UseCaseAdd{r},
		UseCaseComplete{r},
		UseCaseGetList{r},
		UseCaseUpdate{r},
	}
}

type UseCaseDelete struct {
	model.Repository
}

func (cd UseCaseDelete) Delete(ID int) error {
	err := cd.DeleteTask(ID)
	if err != nil {
		return err
	}
	return nil
}

type UseCaseUpdate struct {
	model.Repository
}

func (cu UseCaseUpdate) Update(ID int, title, text string) error {
	err := cu.UpdateTask(ID, title, text)
	if err != nil {
		return err
	}
	return nil
}

type UseCaseAdd struct {
	model.Repository
}

func (ca UseCaseAdd) AddTask(task *model.Task) error {
	err := ca.CreateTask(task)
	if err != nil {
		return err
	}
	return nil
}

type UseCaseGetList struct {
	model.Repository
}

func (cg UseCaseGetList) GetListTasks() ([]model.Task, error) {
	tasks, err := cg.ReadAllTask()
	if err != nil {
		return nil, err
	}
	res := make([]model.Task, len(tasks))
	for i, v := range tasks {
		res[i] = *v
	}
	return res, nil
}

type UseCaseComplete struct {
	model.Repository
}

func (cc UseCaseComplete) Complete(ID int) error {
	err := cc.CompleteTask(ID)
	if err != nil {
		return err
	}
	return nil
}
