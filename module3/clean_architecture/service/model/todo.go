package model

type Task struct {
	Id     int    `json:"id"`
	Title  string `json:"title"`
	Text   string `json:"text"`
	Status string `json:"status"`
}

func (t *Task) GetID() int {
	return t.Id
}

func (t *Task) TaskStatusComplete() {
	t.Status = "complete"
}

func (t *Task) TaskUpdate(title, text string) {
	t.Title = title
	t.Text = text
}

type Repository interface {
	DeleteTask(ID int) error
	UpdateTask(ID int, title, text string) error
	CreateTask(task *Task) error
	ReadAllTask() ([]*Task, error)
	CompleteTask(ID int) error
}

type ToDoService interface {
	Getter
	Adder
	Deleter
	Updater
	Completer
}

type Deleter interface {
	Delete(ID int) error
}
type Updater interface {
	Update(ID int, title, text string) error
}
type Getter interface {
	GetListTasks() ([]Task, error)
}
type Completer interface {
	Complete(ID int) error
}
type Adder interface {
	AddTask(task *Task) error
}
