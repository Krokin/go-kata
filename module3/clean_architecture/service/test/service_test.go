package test

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/Krokin/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/Krokin/go-kata/module3/clean_architecture/service/service"
)

func NewMok() model.Repository {
	return &fakeRep{}
}

type fakeRep struct{}

func (f fakeRep) DeleteTask(ID int) error {
	if ID > 3 || ID < 1 {
		return errors.New("not found")
	}
	return nil
}
func (f fakeRep) CreateTask(task *model.Task) error {
	if task.Id < 4 && task.Id > 0 {
		return errors.New("task with this id exists")
	}
	return nil
}
func (f fakeRep) CompleteTask(ID int) error {
	if ID > 3 || ID < 1 {
		return errors.New("not found")
	}
	return nil
}
func (f fakeRep) UpdateTask(ID int, title, text string) error {
	if ID > 3 || ID < 1 {
		return errors.New("not found")
	}
	return nil
}
func (f fakeRep) ReadAllTask() ([]*model.Task, error) {
	return []*model.Task{{Id: 1, Title: "", Text: "", Status: ""}, {Id: 2, Title: "", Text: "", Status: ""}, {Id: 3, Title: "", Text: "", Status: ""}}, nil
}

func TestUseCaseAdd_AddTask(t *testing.T) {
	tests := []struct {
		name       string
		Repository model.Repository
		task       *model.Task
		wantErr    bool
	}{
		{
			"Test add task ok",
			NewMok(),
			&model.Task{Id: 5, Title: "", Text: "", Status: ""},
			false,
		},
		{
			"Test add task exist",
			NewMok(),
			&model.Task{Id: 2, Title: "", Text: "", Status: ""},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ca := service.UseCaseAdd{
				Repository: tt.Repository,
			}
			if err := ca.AddTask(tt.task); (err != nil) != tt.wantErr {
				t.Errorf("AddTask() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUseCaseComplete_Complete(t *testing.T) {
	tests := []struct {
		name       string
		Repository model.Repository
		task       *model.Task
		wantErr    bool
	}{
		{
			"Test complete task ok",
			NewMok(),
			&model.Task{Id: 2, Title: "", Text: "", Status: ""},
			false,
		},
		{
			"Test complete task not found",
			NewMok(),
			&model.Task{Id: -1, Title: "", Text: "", Status: ""},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cc := service.UseCaseComplete{
				Repository: tt.Repository,
			}
			if err := cc.Complete(tt.task.Id); (err != nil) != tt.wantErr {
				t.Errorf("Complete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUseCaseDelete_Delete(t *testing.T) {
	tests := []struct {
		name       string
		Repository model.Repository
		task       *model.Task
		wantErr    bool
	}{
		{
			"Test delete task ok",
			NewMok(),
			&model.Task{Id: 2, Title: "", Text: "", Status: ""},
			false,
		},
		{
			"Test delete task not found",
			NewMok(),
			&model.Task{Id: 6, Title: "", Text: "", Status: ""},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cd := service.UseCaseDelete{
				Repository: tt.Repository,
			}
			if err := cd.Delete(tt.task.Id); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUseCaseGetList_GetListTasks(t *testing.T) {
	tests := []struct {
		name    string
		fields  model.Repository
		want    []model.Task
		wantErr bool
	}{
		{
			"Test get list tasks",
			NewMok(),
			[]model.Task{{Id: 1, Title: "", Text: "", Status: ""}, {Id: 2, Title: "", Text: "", Status: ""}, {Id: 3, Title: "", Text: "", Status: ""}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cg := service.UseCaseGetList{
				Repository: tt.fields,
			}
			got, err := cg.GetListTasks()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetListTasks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetListTasks() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUseCaseUpdate_Update(t *testing.T) {
	type args struct {
		id    int
		title string
		text  string
	}
	tests := []struct {
		name       string
		Repository model.Repository
		args       args
		wantErr    bool
	}{
		{
			"Test update task ok",
			NewMok(),
			args{
				id:    2,
				title: "",
				text:  "",
			},
			false,
		},
		{
			"Test update task not found",
			NewMok(),
			args{
				id:    0,
				title: "",
				text:  "",
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cu := service.UseCaseUpdate{
				Repository: tt.Repository,
			}
			if err := cu.Update(tt.args.id, tt.args.title, tt.args.text); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
