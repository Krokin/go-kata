package test

import (
	"io"
	"os"
	"reflect"
	"testing"

	"gitlab.com/Krokin/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/Krokin/go-kata/module3/clean_architecture/service/repo"
)

func TestToDoRepository_CompleteTask(t1 *testing.T) {
	type fields struct {
		filepath string
		content  string
	}
	type args struct {
		ID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			"Test complete task repo ok",
			fields{"test.json", `[{"id":1,"title":"1","text":"1","status":"add"}]`},
			args{1},
			[]byte(`[{"id":1,"title":"1","text":"1","status":"complete"}]`),
			false,
		}, {
			"Test complete task repo not found",
			fields{"test.json", `[{"id":1,"title":"1","text":"1","status":"add"}]`},
			args{2},
			[]byte(`[{"id":1,"title":"1","text":"1","status":"add"}]`),
			true,
		}, {
			"Test complete task repo error file",
			fields{"test", ``},
			args{1},
			[]byte(``),
			true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			f, _ := os.Create("test.json")
			defer f.Close()
			_, err := f.Write([]byte(tt.fields.content))
			_ = err
			t := repo.NewFileRepository(tt.fields.filepath)
			if err := t.CompleteTask(tt.args.ID); (err != nil) != tt.wantErr {
				t1.Errorf("CompleteTask() error = %v, wantErr %v", err, tt.wantErr)
			}
			b := make([]byte, 1000)
			_, err = f.Seek(0, io.SeekStart)
			_ = err
			n, _ := f.Read(b)
			if !reflect.DeepEqual(b[:n], tt.want) {
				t1.Errorf("CompleteTask() got = %v, want %v", string(b[:n]), string(tt.want))
			}
		})
	}
}

func TestToDoRepository_CreateTask(t1 *testing.T) {
	type fields struct {
		filepath string
		content  string
	}
	type args struct {
		*model.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			"Test create task ok",
			fields{"test.json", `[{"id":1,"title":"1","text":"1","status":"add"}]`},
			args{&model.Task{
				Id:     2,
				Title:  "sss",
				Text:   "sad",
				Status: "add",
			}},
			[]byte(`[{"id":1,"title":"1","text":"1","status":"add"},{"id":2,"title":"sss","text":"sad","status":"add"}]`),
			false,
		}, {
			"Test create task err exist id",
			fields{"test.json", `[{"id":2,"title":"1","text":"1","status":"add"}]`},
			args{&model.Task{
				Id:     2,
				Title:  "sss",
				Text:   "sad",
				Status: "add",
			}},
			[]byte(`[{"id":2,"title":"1","text":"1","status":"add"}]`),
			true,
		}, {
			"Test create task err rep",
			fields{"test", ``},
			args{&model.Task{
				Id:     2,
				Title:  "sss",
				Text:   "sad",
				Status: "add",
			}},
			[]byte(``),
			true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			f, _ := os.Create("test.json")
			defer f.Close()
			_, err := f.Write([]byte(tt.fields.content))
			_ = err
			t := repo.NewFileRepository(tt.fields.filepath)
			if err := t.CreateTask(tt.args.Task); (err != nil) != tt.wantErr {
				t1.Errorf("CreateTask() error = %v, wantErr %v", err, tt.wantErr)
			}
			b := make([]byte, 1000)
			_, err = f.Seek(0, io.SeekStart)
			_ = err
			n, _ := f.Read(b)
			if !reflect.DeepEqual(b[:n], tt.want) {
				t1.Errorf("CreateTask() got = %v, want %v", string(b[:n]), string(tt.want))
			}
		})
	}
}

func TestToDoRepository_DeleteTask(t1 *testing.T) {
	type fields struct {
		filepath string
		content  string
	}
	type args struct {
		ID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			"Test delete task repo ok",
			fields{"test.json", `[{"id":1,"title":"1","text":"1","status":"add"}]`},
			args{1},
			[]byte(`[]`),
			false,
		}, {
			"Test delete task repo not found",
			fields{"test.json", `[{"id":1,"title":"1","text":"1","status":"add"}]`},
			args{2},
			[]byte(`[{"id":1,"title":"1","text":"1","status":"add"}]`),
			true,
		}, {
			"Test delete task repo error file",
			fields{"test", ``},
			args{1},
			[]byte(``),
			true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			f, _ := os.Create("test.json")
			defer f.Close()
			_, err := f.Write([]byte(tt.fields.content))
			_ = err
			t := repo.NewFileRepository(tt.fields.filepath)
			if err := t.DeleteTask(tt.args.ID); (err != nil) != tt.wantErr {
				t1.Errorf("DeleteTask() error = %v, wantErr %v", err, tt.wantErr)
			}
			b := make([]byte, 1000)
			_, err = f.Seek(0, io.SeekStart)
			_ = err
			n, _ := f.Read(b)
			if !reflect.DeepEqual(b[:n], tt.want) {
				t1.Errorf("DeleteTask() got = %v, want %v", string(b[:n]), string(tt.want))
			}
		})
	}
}

func TestToDoRepository_ReadAllTask(t1 *testing.T) {
	type fields struct {
		filepath string
		content  string
	}
	tests := []struct {
		name    string
		fields  fields
		want    []*model.Task
		wantErr bool
	}{
		{
			"Test ReadAllTask ok",
			fields{"test.json", `[{"id":1,"title":"1","text":"1","status":"add"}]`},
			[]*model.Task{{Id: 1, Title: "1", Text: "1", Status: "add"}},
			false,
		}, {
			"Test ReadAllTask file err",
			fields{"test", `[{"id":1,"title":"1","text":"1","status":"add"}]`},
			nil,
			true,
		}, {
			"Test ReadAllTask file encode err",
			fields{"test", `[{"id"asd:1,"txzcitle":"1"sc,"text":"1","status":"add"}]`},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			f, _ := os.Create("test.json")
			defer f.Close()
			_, err := f.Write([]byte(tt.fields.content))
			_ = err
			t := repo.NewFileRepository(tt.fields.filepath)
			got, err := t.ReadAllTask()
			if (err != nil) != tt.wantErr {
				t1.Errorf("ReadAllTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("ReadAllTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToDoRepository_UpdateTask(t1 *testing.T) {
	type fields struct {
		filepath string
		content  string
	}
	type args struct {
		ID    int
		Title string
		Text  string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			"Test update task repo ok",
			fields{"test.json", `[{"id":1,"title":"1","text":"1","status":"add"}]`},
			args{1, "2", "2"},
			[]byte(`[{"id":1,"title":"2","text":"2","status":"add"}]`),
			false,
		}, {
			"Test update task repo not found",
			fields{"test.json", `[{"id":1,"title":"1","text":"1","status":"add"}]`},
			args{2, "sd", "as"},
			[]byte(`[{"id":1,"title":"1","text":"1","status":"add"}]`),
			true,
		}, {
			"Test update task repo error file",
			fields{"test", ``},
			args{1, "", ""},
			[]byte(``),
			true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			f, _ := os.Create("test.json")
			defer f.Close()
			_, err := f.Write([]byte(tt.fields.content))
			_ = err
			t := repo.NewFileRepository(tt.fields.filepath)
			if err := t.UpdateTask(tt.args.ID, tt.args.Title, tt.args.Text); (err != nil) != tt.wantErr {
				t1.Errorf("UpdateTask() error = %v, wantErr %v", err, tt.wantErr)
			}
			b := make([]byte, 1000)
			_, err = f.Seek(0, io.SeekStart)
			_ = err
			n, _ := f.Read(b)
			if !reflect.DeepEqual(b[:n], tt.want) {
				t1.Errorf("DeleteTask() got = %v, want %v", string(b[:n]), string(tt.want))
			}
		})
	}
}
