package repository

import (
	"encoding/json"
	"errors"
	"io"
	"log"
)

// методы этого интерфейса должны быть со структурой User или интерфейсами
type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

// Я так понял мы можем получить не только файл, но и любой другой объект, но в задании именно про файл написано
// +ReadWriter позволяет один раз прочесть файл и чтобы где-то хранить данные понадобился кэш, без него разве возможно сделать?
type UserRepository struct {
	File  io.ReadWriter
	cache []User
}

// почему тут мы возвращаем не интерфейс Repository, а *UserRepository ведь слои общаются через интерфейсы
func NewUserRepository(file io.ReadWriter) *UserRepository {
	cache := []User{}
	if err := json.NewDecoder(file).Decode(&cache); err != io.EOF {
		log.Println(err)
	}
	return &UserRepository{
		File:  file,
		cache: cache,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// мы получаем record, нужно ли проверять чтобы это была именно структура юзер?
func (r *UserRepository) Save(record interface{}) error {
	data, ok := record.(User)
	if !ok {
		return errors.New("record is not User")
	}
	r.cache = append(r.cache, data)
	err := json.NewEncoder(r.File).Encode(r.cache)
	if err != nil {
		return err
	}
	return nil
}

// если работаем не только с User, то как искать именно по id?
func (r *UserRepository) Find(id int) (interface{}, error) {
	if id < 0 {
		return nil, errors.New("not valid id")
	}
	if len(r.cache) < 1 {
		return nil, errors.New("empty repository")
	}
	for _, i := range r.cache {
		if i.ID == id {
			return i, nil
		}
	}
	return nil, errors.New("not found")
}

// Возвращаем слайс интерфейсов или все-таки  []User
func (r *UserRepository) FindAll() ([]interface{}, error) {
	if len(r.cache) < 1 {
		return nil, errors.New("empty repository")
	}
	var res []interface{}
	for _, v := range r.cache {
		res = append(res, v)
	}
	return res, nil
}
