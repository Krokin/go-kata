package repository

import (
	"bytes"
	"io"
	"reflect"
	"testing"
)

func TestUserRepository_Find(t *testing.T) {
	type fields struct {
		File  io.ReadWriter
		cache []User
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name:    "Test Find bad id",
			fields:  fields{},
			args:    args{-1},
			want:    nil,
			wantErr: true,
		}, {
			name: "Test Find no user",
			fields: fields{
				File:  bytes.NewBuffer([]byte(`[{"id":2,"name":"sd"}]`)),
				cache: []User{{2, "sd"}},
			},
			args:    args{1},
			want:    nil,
			wantErr: true,
		}, {
			name: "Test Find in empty repo",
			fields: fields{
				File:  bytes.NewBuffer([]byte(``)),
				cache: []User{},
			},
			args:    args{5},
			want:    nil,
			wantErr: true,
		}, {
			name: "Test Find ok",
			fields: fields{
				File:  bytes.NewBuffer([]byte(`[{"id":2,"name":"sd"},{"id":5,"name":"ssd"}]`)),
				cache: []User{{2, "sd"}, {5, "ssd"}},
			},
			args:    args{5},
			want:    User{5, "ssd"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File:  tt.fields.File,
				cache: tt.fields.cache,
			}
			got, err := r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	type fields struct {
		File  io.ReadWriter
		cache []User
	}
	tests := []struct {
		name    string
		fields  fields
		want    []interface{}
		wantErr bool
	}{
		{
			name: "Test FindAll in empty repo",
			fields: fields{
				File:  bytes.NewBuffer([]byte(``)),
				cache: []User{},
			},
			want:    nil,
			wantErr: true,
		}, {
			name: "Test FindAll ok",
			fields: fields{
				File:  bytes.NewBuffer([]byte(`[{"id":2,"name":"sd"},{"id":5,"name":"ssd"}]`)),
				cache: []User{{2, "sd"}, {5, "ssd"}},
			},
			want:    []interface{}{User{2, "sd"}, User{5, "ssd"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File:  tt.fields.File,
				cache: tt.fields.cache,
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_Save(t *testing.T) {
	type fields struct {
		File  io.ReadWriter
		cache []User
	}
	type args struct {
		record interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "Test Save data is not user",
			fields: fields{
				File:  bytes.NewBuffer([]byte(``)),
				cache: []User{{2, "sd"}},
			},
			args:    args{-1},
			want:    nil,
			wantErr: true,
		}, {
			name: "Test Save ok",
			fields: fields{
				File:  bytes.NewBuffer([]byte(``)),
				cache: []User{},
			},
			args:    args{User{1, "sd"}},
			want:    []byte(`[{"id":1,"name":"sd"}]`),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File:  tt.fields.File,
				cache: tt.fields.cache,
			}
			if err := r.Save(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
			b := make([]byte, 100)
			n, _ := r.File.Read(b)
			if tt.want != nil {
				if !reflect.DeepEqual(b[:n-1], tt.want) {
					t.Errorf("Save() got = %s, want %s", b[:n-1], tt.want)
				}
			}
		})
	}
}
