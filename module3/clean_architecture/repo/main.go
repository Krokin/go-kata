package main

import (
	"log"
	"os"

	"gitlab.com/Krokin/go-kata/module3/clean_architecture/repo/repository"
)

func main() {
	f, err := os.OpenFile("repository.json", os.O_RDWR, 0777)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	repository.NewUserRepository(f)
}
