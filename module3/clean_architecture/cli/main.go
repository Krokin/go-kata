package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"

	m "gitlab.com/Krokin/go-kata/module3/clean_architecture/cli/model"
	r "gitlab.com/Krokin/go-kata/module3/clean_architecture/cli/repo"
	s "gitlab.com/Krokin/go-kata/module3/clean_architecture/cli/service"
)

type todo struct {
	s m.ToDoService
}

type ToDo interface {
	update()
	delete()
	add()
	read()
	complete()
}

func NewToDo(service m.ToDoService) ToDo {
	return todo{service}
}

// ID пишется, потому что я не делал инкрементацию, как в бд, поэтому в этой реализации юзер сам отвечает за ID)
// Добавил в мейне создание файла
func main() {
	f, err := os.Create("repo.json")
	if err != nil {
		log.Fatalln(err)
	}
	f.Close()
	r := r.NewFileRepository("repo.json")
	srvc := s.NewToDoService(r)
	t := NewToDo(srvc)
	menu(t)
}

func menu(t ToDo) {
	fmt.Println("Welcome in ToDo list!")
loop:
	for {
		fmt.Printf("1.Показать список задач\n2.Добавить задачу\n3.Удалить задачу\n")
		fmt.Printf("4.Редактировать задачу по ID\n5.Завершить задачу\n6.Exit\n")
		var a int
		_, err := fmt.Scan(&a)
		if err != nil {
			fmt.Println("Ввод нераспознан")
			clear()
			continue
		}
		switch a {
		case 1:
			t.read()
		case 2:
			t.add()
		case 3:
			t.delete()
		case 4:
			t.update()
		case 5:
			t.complete()
		case 6:
			fmt.Println("Good luck!")
			break loop
		default:
			fmt.Println("Ввод нераспознан")
		}
	}
}

func (t todo) read() {
	tasks, err := t.s.GetListTasks()
	if err != nil {
		log.Println(err)
		return
	}
	if len(tasks) < 1 {
		fmt.Println("Задач нет")
	}
	for _, t := range tasks {
		fmt.Printf("ID %d: %s\n %s\nStatus: %s\n", t.Id, t.Title, t.Text, t.Status)
	}
}
func (t todo) complete() {
	var id int
	for {
		fmt.Println("Введите ID")
		_, err := fmt.Scan(&id)
		if err != nil {
			fmt.Println("Ввод нераспознан")
			clear()
			continue
		}
		break
	}
	err := t.s.Complete(id)
	if err != nil {
		log.Println(err)
	} else {
		fmt.Println("Успешное выполнение")
	}
}

func (t todo) update() {
	fmt.Println("ID для изменения")
	id, title, text := scanData()
	err := t.s.Update(id, title, text)
	if err != nil {
		log.Println(err)
	} else {
		fmt.Println("Успешное обновление")
	}
}

func (t todo) delete() {
	var id int
	for {
		fmt.Println("Введите: ID")
		_, err := fmt.Scanln(&id)
		if err == nil {
			break
		}
		fmt.Println("Ввод нераспознан")
		clear()
	}
	err := t.s.Delete(id)
	if err != nil {
		log.Println(err)
	} else {
		fmt.Println("Успешное удаление")
	}

}
func (t todo) add() {
	fmt.Println("ID для добавления")
	id, title, text := scanData()
	task := &m.Task{
		Id:     id,
		Title:  title,
		Text:   text,
		Status: "add",
	}
	err := t.s.AddTask(task)
	if err != nil {
		log.Println(err)
	} else {
		fmt.Println("Успешное добавление")
	}
}

func scanData() (int, string, string) {
	var (
		id          int
		title, text string
	)
	s := bufio.NewReader(os.Stdin)
	for {
		fmt.Println("Введите: ID")
		str, _ := s.ReadString('\n')
		num, err := strconv.Atoi(str[:len(str)-1])
		if err != nil {
			fmt.Println("Ввод нераспознан")
			continue
		}
		id = num
		fmt.Println("Введите: название задачи")
		title, err = s.ReadString('\n')
		if err != nil {
			fmt.Println("Ввод нераспознан")
			continue
		}
		fmt.Println("Введите: текст задачи")
		text, err = s.ReadString('\n')
		if err != nil {
			fmt.Println("Ввод нераспознан")
			continue
		}
		break
	}
	fmt.Println(title, text)
	return id, title[:len(title)-1], text[:len(text)-1]
}

func clear() {
	var discard string
	fmt.Scanln(&discard)
}
