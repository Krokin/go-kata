package main

import (
	"reflect"
	"testing"
	"time"
)

func TestDoubleLinkedList_Append(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name string
		arg  Commit
	}{
		{
			"Append normal Commit",
			Commit{Message: "We need to program the bluetooth ADP protocol!"},
		}, {
			"Append empty Commit",
			Commit{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d.Append(tt.arg)
			if d.tail.data.Message != tt.arg.Message {
				t.Errorf("Append err")
			}
		})
	}
}

func TestDoubleLinkedList_Current(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name string
		list DoubleLinkedList
		walk int
		want *Node
	}{
		{
			"Current first elem",
			d,
			0,
			d.head,
		}, {
			"Current last elem",
			d,
			1,
			d.tail,
		}, {
			"Current empty list",
			DoubleLinkedList{},
			0,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i := 0; i < tt.walk; i++ {
				tt.list.Next()
			}
			if got := tt.list.Current(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Current() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(5)
	err := d.LoadData("genJSON.json")
	_ = err
	first := d.head.data.Message
	last := d.tail.data.Message
	d.Next()
	d.Next()
	middle := d.curr.data.Message
	tests := []struct {
		name       string
		fields     DoubleLinkedList
		arg        int
		messageDel string
		wantLen    int
		wantErr    bool
	}{
		{
			"Delete empty list",
			DoubleLinkedList{},
			0,
			"",
			0,
			true,
		}, {
			"Delete negate n",
			d,
			-1,
			"",
			5,
			true,
		}, {
			"Delete n out range list",
			d,
			20,
			"",
			5,
			true,
		}, {
			"Delete first",
			d,
			0,
			first,
			4,
			false,
		}, {
			"Delete last",
			d,
			4,
			last,
			4,
			false,
		}, {
			"Delete middle",
			d,
			2,
			middle,
			4,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.fields.Delete(tt.arg)
			if (err != nil) != tt.wantErr {
				t.Error("error Delete")
			}
			if tt.fields.len != tt.wantLen {
				t.Errorf("error Delete len: want %d, fact %d", tt.wantLen, tt.fields.len)
			}
			if n := tt.fields.Search(tt.messageDel); n != nil {
				t.Error("error Delete check search")
			}
		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON")
	_ = err
	tests := []struct {
		name string
		list DoubleLinkedList
		want *Node
	}{
		{
			"DeleteCurrent empty list",
			DoubleLinkedList{},
			d.Current(),
		}, {
			"DeleteCurrent normal list",
			d,
			d.Current(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.list.DeleteCurrent()
			if tt.want == nil && tt.list.curr != nil {
				t.Error("Delete Current error")
			} else if tt.list.curr != nil {
				if got := tt.list.Search(tt.want.data.Message); got != nil {
					t.Error("Delete Current error")
				}
			}
		})
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name string
		list DoubleLinkedList
		walk int
		want int
	}{
		{
			"Index first elem",
			d,
			0,
			0,
		}, {
			"Index last elem",
			d,
			1,
			1,
		}, {
			"Index empty list",
			DoubleLinkedList{},
			0,
			0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i := 0; i < tt.walk; i++ {
				tt.list.Next()
			}
			if got, _ := tt.list.Index(); got != tt.want {
				t.Errorf("Index() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	d := &DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	type args struct {
		n int
		c Commit
	}
	tests := []struct {
		name    string
		fields  *DoubleLinkedList
		args    args
		wantErr bool
		wantLen int
	}{
		{
			"Insert empty list",
			&DoubleLinkedList{},
			args{0, Commit{"214", "2141r", time.Now()}},
			true,
			0,
		}, {
			"Insert first",
			d,
			args{0, Commit{"asd", "Asd", time.Now()}},
			false,
			3,
		}, {
			"Insert last",
			d,
			args{1, Commit{"asd1", "Asd", time.Now()}},
			false,
			3,
		}, {
			"Insert empty commit",
			d,
			args{1, Commit{}},
			false,
			3,
		}, {
			"Insert out range list",
			d,
			args{20, Commit{"asd4", "Asd", time.Now()}},
			true,
			2,
		}, {
			"Insert n negate list",
			d,
			args{-1, Commit{"asd5", "Asd", time.Now()}},
			true,
			2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.fields.Insert(tt.args.n, tt.args.c)
			if (err != nil) != tt.wantErr {
				t.Error("err err")
			}
			if tt.fields.Len() != tt.wantLen {
				t.Error("err len")
			}
			if n := tt.fields.Search(tt.args.c.Message); n == nil && !tt.wantErr {
				t.Error("err not found")
			}
			if err = tt.fields.Delete(tt.args.n + 1); err != nil && !tt.wantErr {
				t.Error("err index insert")
			}

		})
	}
}

func TestDoubleLinkedList_Len(t *testing.T) {
	GenerateJSON(2)
	oneD := DoubleLinkedList{}
	err := oneD.LoadData("genJSON.json")
	_ = err
	GenerateJSON(2000)
	twoD := DoubleLinkedList{}
	err = twoD.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name   string
		fields DoubleLinkedList
		want   int
	}{
		{
			"Two len",
			oneD,
			2,
		}, {
			"Zero len",
			DoubleLinkedList{},
			0,
		}, {
			"Big data len",
			twoD,
			2000,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fields.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_LoadData(t *testing.T) {
	tests := []struct {
		name    string
		gen     int
		fields  DoubleLinkedList
		wantLen int
		wantErr bool
	}{
		{
			"Load empty file",
			0,
			DoubleLinkedList{},
			0,
			true,
		}, {
			"Load normal file",
			2,
			DoubleLinkedList{},
			2,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			GenerateJSON(tt.gen)
			if err := tt.fields.LoadData("genJSON.json"); (err != nil) != tt.wantErr {
				t.Errorf("LoadData() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.fields.len != tt.wantLen {
				t.Errorf("LoadData() len = %v, wantLen %v", tt.fields.len, tt.wantLen)
			}

		})
	}
}

func TestDoubleLinkedList_Next(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name string
		list DoubleLinkedList
		walk int
		want *Node
	}{
		{
			"Next first elem",
			d,
			0,
			d.head.next,
		}, {
			"Next last elem",
			d,
			1,
			nil,
		}, {
			"Next empty list",
			DoubleLinkedList{},
			0,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i := 0; i < tt.walk; i++ {
				tt.list.curr = tt.list.curr.next
			}
			if got := tt.list.Next(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name   string
		fields DoubleLinkedList
		want   *Node
	}{
		{
			"Pop empty list",
			DoubleLinkedList{},
			nil,
		}, {
			"Pop normal list",
			d,
			d.tail.prev,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			len := tt.fields.len
			if got := tt.fields.Pop(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pop() = %v, want %v", got, tt.want)
			}
			if len != 0 && len-1 != tt.fields.len {
				t.Errorf("Len = %v, want %v", len-1, tt.fields.len)
			}
		})
	}
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name string
		list DoubleLinkedList
		walk int
		want *Node
	}{
		{
			"Prev first elem",
			d,
			0,
			nil,
		}, {
			"Prev last elem",
			d,
			1,
			d.tail.prev,
		}, {
			"Prev empty list",
			DoubleLinkedList{},
			0,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i := 0; i < tt.walk; i++ {
				tt.list.Next()
			}
			if got := tt.list.Prev(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Prev() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name   string
		first  *Node
		last   *Node
		fields DoubleLinkedList
	}{
		{
			"Reverse normal list",
			d.head,
			d.tail,
			d,
		}, {
			"Reverse empty list",
			nil,
			nil,
			DoubleLinkedList{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.fields.Reverse()
			if !reflect.DeepEqual(got.tail, tt.first) || !reflect.DeepEqual(got.head, tt.last) {
				t.Errorf("error Reverse()")
			}
		})
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name   string
		fields DoubleLinkedList
		arg    string
		want   *Node
	}{
		{
			"Search Test empty arg",
			d,
			"",
			nil,
		}, {
			"Search Test normal arg",
			d,
			d.head.data.Message,
			&Node{data: &Commit{Message: d.head.data.Message}},
		}, {
			"Search Test not in list",
			d,
			"AAAAAA@@@",
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := d.Search(tt.arg)
			if tt.want != got {
				if tt.want != nil && got != nil {
					if tt.want.data.Message != got.data.Message {
						t.Errorf("Search() = %v, want %v", got, tt.want)
					}
				} else {
					t.Errorf("Search() = %v, want %v", got, tt.want)
				}
			}
		})
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name   string
		fields DoubleLinkedList
		arg    string
		want   *Node
	}{
		{
			"SearchUUID Test empty arg",
			d,
			"",
			nil,
		}, {
			"SearchUUID Test normal arg",
			d,
			d.head.data.UUID,
			&Node{data: &Commit{UUID: d.head.data.UUID}},
		}, {
			"SearchUUID Test not in list",
			d,
			"AAAAAA@@@",
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := d.SearchUUID(tt.arg)
			if tt.want != got {
				if tt.want != nil && got != nil {
					if tt.want.data.UUID != got.data.UUID {
						t.Errorf("Search() = %v, want %v", got, tt.want)
					}
				} else {
					t.Errorf("Search() = %v, want %v", got, tt.want)
				}
			}
		})
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	d := DoubleLinkedList{}
	GenerateJSON(2)
	err := d.LoadData("genJSON.json")
	_ = err
	tests := []struct {
		name   string
		fields DoubleLinkedList
		want   *Node
	}{
		{
			"Shift empty list",
			DoubleLinkedList{},
			nil,
		}, {
			"Shift normal list",
			d,
			d.head.next,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			len := tt.fields.len
			if got := tt.fields.Shift(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Shift() = %v, want %v", got, tt.want)
			}
			if len != 0 && len-1 != tt.fields.len {
				t.Errorf("Len = %v, want %v", len-1, tt.fields.len)
			}

		})
	}
}

func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := v.Delete(0)
		_ = err
	}
}

func BenchmarkDoubleLinkedList_DeleteCurrent(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.DeleteCurrent()
	}
}

func BenchmarkDoubleLinkedList_Pop(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.Pop()
	}
}

func BenchmarkDoubleLinkedList_Shift(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.Shift()
	}
}

func BenchmarkDoubleLinkedList_Append(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	c := Commit{"Asd", "sad", time.Now()}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.Append(c)
	}
}

func BenchmarkDoubleLinkedList_Current(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.Current()
	}
}

func BenchmarkDoubleLinkedList_Len(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.Len()
	}
}

func BenchmarkDoubleLinkedList_Next(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.Next()
	}
}

func BenchmarkDoubleLinkedList_Prev(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.Prev()
	}
}

func BenchmarkDoubleLinkedList_Reverse(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v = *v.Reverse()
	}
}

func BenchmarkDoubleLinkedList_Search(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.Search("Calculating the driver won't do anything, we need to index the haptic USB bus!")
	}
}

func BenchmarkDoubleLinkedList_SearchUUID(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		v.SearchUUID("6958201a-875b-11ed-8150-acde48001122")
	}
}

func BenchmarkDoubleLinkedList_LoadData(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := v.LoadData("genJSON.json")
		_ = err
	}
}

func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	c := Commit{"AD", "zv", time.Now()}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := v.Insert(v.len/2, c)
		_ = err
	}
}

func BenchmarkDoubleLinkedList_Index(b *testing.B) {
	v := DoubleLinkedList{}
	GenerateJSON(2000)
	err := v.LoadData("genJSON.json")
	_ = err
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := v.Index()
		_ = err
	}
}

func BenchmarkGenerateJSON(b *testing.B) {
	for i := 0; i < b.N; i++ {
		GenerateJSON(1000)
	}
}
