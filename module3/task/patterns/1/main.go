package main

import "fmt"

type PricingStrategy interface {
	Calculate(Order) float64
	Discount() string
}

type Order struct {
	name  string
	price float64
	count int
}

type RegularPricing struct{}

func (r RegularPricing) Calculate(o Order) float64 {
	return o.price * float64(o.count)
}

func (r RegularPricing) Discount() string {
	return "Regular price"
}

type SalePricing struct {
	nameDiscount string
	percent      float64
}

func (s SalePricing) Calculate(o Order) float64 {
	return o.price * (1 - (s.percent / 100)) * float64(o.count)
}
func (s SalePricing) Discount() string {
	return s.nameDiscount
}

func main() {
	s := []PricingStrategy{
		RegularPricing{},
		SalePricing{
			nameDiscount: "Скидка постоянного покупателя",
			percent:      5,
		},
		SalePricing{
			nameDiscount: "Скидка весеннего сезона",
			percent:      10,
		},
		SalePricing{
			nameDiscount: "Новогодняя распродажа",
			percent:      50,
		},
	}
	order := Order{
		name:  "Nike Jordan",
		price: 8000,
		count: 10,
	}
	for _, v := range s {
		fmt.Printf("Total cost order: %.2f, with %s\n", v.Calculate(order), v.Discount())
	}
}
