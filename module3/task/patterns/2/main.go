package main

import (
	"errors"
	"fmt"
	"time"
)

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type ControlConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int) int
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct {
	status      bool
	temperature int
}

func (r *RealAirConditioner) Status() bool {
	return r.status
}

func (r *RealAirConditioner) On() {
	if !r.status {
		r.status = true
	}
}

func (r *RealAirConditioner) Off() {
	if r.status {
		r.status = false
	}
}

func (r *RealAirConditioner) Temperature(temp int) {
	r.temperature = temp
}

type ConditionerAdapter struct {
	cond *RealAirConditioner
}

func NewConditionerAdapter() *ConditionerAdapter {
	return &ConditionerAdapter{cond: &RealAirConditioner{
		status:      false,
		temperature: 17,
	}}
}

func (c ConditionerAdapter) TurnOn() {
	if !c.cond.Status() {
		c.cond.On()
	}
}

func (c ConditionerAdapter) TurnOff() {
	if c.cond.Status() {
		c.cond.Off()
	}
}

func (c ConditionerAdapter) SetTemperature(temp int) int {
	if temp < 10 {
		c.cond.Temperature(10)
		temp = 10
	} else if temp > 32 {
		c.cond.Temperature(32)
		temp = 32
	} else {
		c.cond.Temperature(temp)
	}
	return temp
}

type Log struct {
	date   string
	status string
}

type ConditionerProxy struct {
	cond       ControlConditioner
	authList   map[string]struct{}
	logs       []Log
	authStatus bool
}

func NewAirConditionerProxy() *ConditionerProxy {
	return &ConditionerProxy{
		cond:       NewConditionerAdapter(),
		authList:   make(map[string]struct{}),
		logs:       []Log{},
		authStatus: false,
	}
}

func (p *ConditionerProxy) newLog(log string) {
	fmt.Println(log)
	p.logs = append(p.logs, Log{
		date:   time.Now().Format(time.RFC822Z),
		status: log,
	})
}

func (p *ConditionerProxy) AuthStatus() bool {
	return p.authStatus
}

func (p *ConditionerProxy) TurnOn() {
	if p.AuthStatus() {
		p.newLog("Turning on the air conditioner")
		p.cond.TurnOn()
	} else {
		p.newLog("Access denied: authentication required to turn on the air conditioner")
	}
}

func (p *ConditionerProxy) TurnOff() {
	if p.AuthStatus() {
		p.newLog("Turning off the air conditioner")
		p.cond.TurnOff()
	} else {
		p.newLog("Access denied: authentication required to turn off the air conditioner")
	}
}

func (p *ConditionerProxy) SetTemperature(temp int) {
	if p.AuthStatus() {
		t := p.cond.SetTemperature(temp)
		p.newLog(fmt.Sprintf("Setting air conditioner temperature to %d", t))
	} else {
		p.newLog("Access denied: authentication required to set the temperature of the air conditioner")
	}
}

func (p *ConditionerProxy) Authentication(login string) error {
	if _, ok := p.authList[login]; !ok {
		p.newLog(fmt.Sprintf("User: %s - not registered", login))
		p.authStatus = false
		return errors.New("user not registered")
	}
	p.authStatus = true
	p.newLog(fmt.Sprintf("Success authentication for user: %s", login))
	return nil
}

func (p *ConditionerProxy) RegistrationUser(login string) error {
	if _, ok := p.authList[login]; ok {
		p.newLog("User already registered")
		return errors.New("user already registered")
	}
	p.authList[login] = struct{}{}
	p.newLog("Success already registered")
	return nil
}

func (p *ConditionerProxy) ReadLogs() {
	if !p.AuthStatus() {
		p.newLog("Access denied: authentication required to read logs")
		return
	}
	for _, l := range p.logs {
		fmt.Printf("%s: %s\n", l.date, l.status)
	}
}

type AirSmartConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
	Auther
}

type Auther interface {
	RegistrationUser(login string) error
	Authentication(login string) error
	ReadLogs()
}

func NewAirSmartConditioner() AirSmartConditioner {
	return NewAirConditionerProxy()
}
func main() {

	airConditioner := NewAirSmartConditioner()
	//not auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
	// with auth
	err := airConditioner.RegistrationUser("admin")
	_ = err
	err = airConditioner.Authentication("admin")
	_ = err
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
	airConditioner.SetTemperature(-24)
	airConditioner.ReadLogs()
}
