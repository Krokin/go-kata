package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
}

type Coordinates struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

func (o *OpenWeatherAPI) getCoordinates(location string, c http.Client) (*Coordinates, error) {
	r, err := c.Get(fmt.Sprintf("http://api.openweathermap.org/geo/1.0/direct?q=%s,RU&appid=%s", location, o.apiKey))
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()
	var res []*Coordinates
	if err = json.NewDecoder(r.Body).Decode(&res); err != nil {
		return nil, err
	}
	return res[0], nil
}

type TempStatus struct {
	Main struct {
		TempKelvins float64 `json:"temp"`
		Humidity    int     `json:"humidity"`
	} `json:"main"`
	Wind struct {
		Speed float64 `json:"speed"`
	} `json:"wind"`
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	c := http.Client{}
	coords, err := o.getCoordinates(location, c)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	r, err := c.Get(fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=%s", coords.Lat, coords.Lon, o.apiKey))
	if err != nil {
		fmt.Println(err)
		return 0
	}
	defer r.Body.Close()
	var res TempStatus
	if err = json.NewDecoder(r.Body).Decode(&res); err != nil {
		fmt.Println(err)
		return 0
	}
	return int(res.Main.TempKelvins - 273.15)
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	c := http.Client{}
	coords, err := o.getCoordinates(location, c)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	r, err := c.Get(fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=%s", coords.Lat, coords.Lon, o.apiKey))
	if err != nil {
		fmt.Println(err)
		return 0
	}
	defer r.Body.Close()
	var res TempStatus
	if err = json.NewDecoder(r.Body).Decode(&res); err != nil {
		fmt.Println(err)
		return 0
	}
	return res.Main.Humidity
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	c := http.Client{}
	coords, err := o.getCoordinates(location, c)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	r, err := c.Get(fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=%s", coords.Lat, coords.Lon, o.apiKey))
	if err != nil {
		fmt.Println(err)
		return 0
	}
	defer r.Body.Close()
	var res TempStatus
	if err = json.NewDecoder(r.Body).Decode(&res); err != nil {
		fmt.Println(err)
		return 0
	}
	return int(res.Wind.Speed)
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("0e1e655746a354c548bddbc895525344")
	cities := []string{"Москва", "Санкт-Петербург", "Казань", "Якутск"}
	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d С\n", temperature)
		fmt.Printf("Humidity in "+city+": %d%%\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d м/с \n\n", windSpeed)
	}
}
