package main

import (
	"encoding/json"
	"errors"
	"math/rand"
	"os"
	"reflect"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	jsoniter "github.com/json-iterator/go"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Append(c Commit)
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Reverse() *DoubleLinkedList
	Search(message string) *Node
	SearchUUID(uuID string) *Node
	Shift() *Node
	Pop() *Node
}

func QuickSort(data []Commit) []Commit {
	if len(data) < 2 {
		return data
	}
	middle := data[rand.Intn(len(data))].Date
	lowS := make([]Commit, 0, len(data))
	highS := make([]Commit, 0, len(data))
	midS := make([]Commit, 0, len(data))
	for _, c := range data {
		switch {
		case middle.Equal(c.Date):
			midS = append(midS, c)
		case middle.Before(c.Date):
			highS = append(highS, c)
		case middle.After(c.Date):
			lowS = append(lowS, c)
		}
	}
	highS = QuickSort(highS)
	lowS = QuickSort(lowS)
	lowS = append(lowS, midS...)
	lowS = append(lowS, highS...)
	return lowS
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	var com []Commit
	err = json.NewDecoder(f).Decode(&com)
	if err != nil {
		return err
	}
	if len(com) == 0 {
		return errors.New("empty file")
	}
	com = QuickSort(com)
	for _, c := range com {
		d.Append(c)
	}
	return nil
}

// Добавляет в конец списка
func (d *DoubleLinkedList) Append(c Commit) {
	node := &Node{data: &c}
	if d.len == 0 {
		d.tail = node
		d.head = node
		d.curr = node
	} else {
		d.tail.next = node
		node.prev = d.tail
		d.tail = node
	}
	d.len++
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil {
		return nil
	}
	res := d.curr.next
	if res != nil {
		d.curr = d.curr.next
	}
	return res
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil {
		return nil
	}
	res := d.curr.prev
	if res != nil {
		d.curr = d.curr.prev
	}
	return res
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if d.Len() == 0 {
		return errors.New("error empty doublelinkedlist")
	}
	if n < 0 || n > d.Len()-1 {
		return errors.New("error valid index")
	}
	idx, err := d.Index()
	if err != nil {
		return err
	}
	if idx > n {
		for i := 0; i < idx-n; i++ {
			d.Prev()
		}
	} else {
		for i := 0; i < n-idx; i++ {
			d.Next()
		}
	}
	cur := d.Current()
	next := d.Next()
	newNode := &Node{&c, cur, next}
	if next != nil {
		next.prev = newNode
	} else {
		d.tail = newNode
	}
	cur.next = newNode
	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if n < 0 || n >= d.Len() {
		return errors.New("not valid index")
	}
	idx, err := d.Index()
	if err != nil {
		return err
	}
	if idx > n {
		for i := 0; i < idx-n; i++ {
			d.Prev()
		}
	} else {
		for i := 0; i < n-idx; i++ {
			d.Next()
		}
	}
	d.DeleteCurrent()
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() {
	if d.Current() == nil {
		return
	}
	cur := d.Current()
	next := cur.next
	prev := cur.prev
	if next != nil {
		next.prev = prev
	}
	if prev != nil {
		prev.next = next
	}
	if reflect.DeepEqual(cur, d.tail) {
		d.tail = prev
	}
	if reflect.DeepEqual(cur, d.head) {
		d.head = next
	}
	d.len--
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.len < 1 {
		return 0, errors.New("empty doublelinkedlist")
	}
	count := 0
	n := d.Prev()
	for n != nil {
		count++
		if reflect.DeepEqual(n, d.curr) {
			break
		}
		n = d.Next()
	}
	return count, nil
}

// Pop Операция Pop удал последний
func (d *DoubleLinkedList) Pop() *Node {
	err := d.Delete(d.Len() - 1)
	_ = err
	return d.tail
}

// Shift операция shift удаляет 1 элемент и возвращает значение
func (d *DoubleLinkedList) Shift() *Node {
	err := d.Delete(0)
	_ = err
	return d.head
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	d.curr = d.head
	n := d.Current()
	for n != nil {
		if n.data.UUID == uuID {
			break
		}
		n = d.Next()
	}
	return n
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	d.curr = d.head
	n := d.Current()
	for n != nil {
		if n.data.Message == message {
			break
		}
		n = d.Next()
	}
	return n
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	d.curr = d.tail
	n := d.Current()
	for n != nil {
		n.next, n.prev = n.prev, n.next
		n = d.Next()
	}
	d.head, d.tail = d.tail, d.head
	return d
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON(n int) {
	list := make([]Commit, 0, n)
	for j := 0; j < n; j++ {
		i := 0
		for i < 5 {
			i = rand.Intn(15)
		}
		list = append(list, Commit{
			Message: gofakeit.Paragraph(1, 1, i, ""),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.Date(),
		})
	}
	f, err := os.Create("genJSON.json")
	if err != nil {
		panic("err: create file err")
	}
	defer f.Close()
	data, err := jsoniter.Marshal(list)
	if err != nil {
		panic("err: marshal err")
	}
	_, err = f.Write(data)
	if err != nil {
		panic("err: write file err")
	}
}
